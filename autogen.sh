#!/bin/sh

gnulib-tool --import --dir=.  --lib=libgnu --source-base=build-aux/gnulib --m4-base=build-aux/m4 --doc-base=doc --tests-base=tests --aux-dir=build-aux  --macro-prefix=gl  alloca getopt-gnu strdup-posix stdint obstack

autoreconf -i
