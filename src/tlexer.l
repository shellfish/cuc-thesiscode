%option noyywrap
%option reentrant
%option extra-type="LexerState *"

%top{
#define TINA_CORE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <assert.h>

#include "auxlib/tstring.h"

#include "ttabs.h"
#include "tina.h"
#include "tast.h"


}

%x   block_comment
%x   squote_string
%x   dquote_string
%x   long_string

NAME [a-zA-Z_][a-zA-Z_0-9]*

HEX	0x[a-f0-9]+
INTEGER [0-9]+
NUMBER_WITH_INTEGER_PART {INTEGER}(\.[0-9]+)?
NUMBER_WITHOUT_INTEGER_PART \.[0-9]+
NUMBER ({NUMBER_WITH_INTEGER_PART}|{NUMBER_WITHOUT_INTEGER_PART})([eE][0-9]+)? 

%%

%{

#ifdef __cplusplus
#define myinput(a) yyinput(a)
#else
#define myinput(a) input(a)
#endif

#define INCREASE_LINENO() do { yyextra->line_number++; } while(0)
#define GET_NEXT_CHAR()  myinput(yyscanner)

#define GET_DEPTH() yyextra->nesting_depth
#define INCREASE_DEPTH() do { GET_DEPTH()++; } while (0)

#define STRING_BUFFER yyextra->string_buffer

#define ADD_BYTE_TO_BUF(c) tinaU_addByte(STRING_BUFFER, (c))
#define ADD_STRING_TO_BUF() tinaU_addStr(STRING_BUFFER, yytext, yyleng)

#define DUMP_CURR_BUF() strdup(STRING_BUFFER->buf)
#define RESET_CURR_BUF() do { STRING_BUFFER->leng = 0; } while (0)

#define LITERAL_POOL yyextra->constant_pool

#define LVAL_STRING yyextra->lval.string
#define LVAL_NAME   yyextra->lval.name
#define LVAL_NUMBER yyextra->lval.number


#define SET_ERRMSG(code) \
    yyextra->error_func(yyextra->error_opaque, tinaE_getstr(code)) \


%}
        /* comment of Lua begins with a double hyphen */
"--"	{do {
            int c = GET_NEXT_CHAR();
            if (c != '[')
                goto line_comment;
            else {
                GET_DEPTH() = 0;
                while ((c = GET_NEXT_CHAR()) == '=')
                    INCREASE_DEPTH();
                if (c == '[') {
                    BEGIN(block_comment);
                    break;
                } else
                    goto line_comment;
            }
line_comment:
            INCREASE_LINENO();
            unput(c);
            while (GET_NEXT_CHAR() != '\n');
            BEGIN(INITIAL);
        }while(0); }

<block_comment>{
    [^]\n]+ /* eat up comment */
        "\n"	INCREASE_LINENO();
    "]"	{do {
        int c = GET_NEXT_CHAR();
        if (c == ']' && GET_DEPTH() == 0) {
            BEGIN(INITIAL);
            break;
        } else if (c == '=') {
            int count = 1;
            while ((c = GET_NEXT_CHAR()) == '=')
                count++;
            if (c == ']' && count == GET_DEPTH()) {
                BEGIN(INITIAL);
                break;
            } else
                unput(c);
        }
    }while(0);}
}

    /* string literal */
\'	BEGIN(squote_string);
\"	BEGIN(dquote_string);
\[	{do{ 

    int c = GET_NEXT_CHAR();
    if (c == '[') {
        GET_DEPTH() = 0;
        goto start;
    } else if (c == '=') {
        GET_DEPTH() = 1;
        while ((c = GET_NEXT_CHAR()) == '=') INCREASE_DEPTH();			
        if (c == '[') 
            goto start;
        else {
            SET_ERRMSG(MSGL_INVALID_OPEN_BRACKET);
            assert(0);
            abort();
        }
    } else {
        unput(c);
        return TK_LEFT_BRACKET;
    }
    break;
start:
    c = GET_NEXT_CHAR();
    /* Lua中， 
     * 长字符串字面值的第一个换行如果紧跟着左[，忽略之
     */
    if (c != '\n') 
        unput(c);
    else
        INCREASE_LINENO();
    BEGIN(long_string);				

}while(0);}

<long_string>{
    [^]\n]+	ADD_STRING_TO_BUF();
    "\n"	INCREASE_LINENO(); ADD_BYTE_TO_BUF('\n');
    \]	{do{
        int c = GET_NEXT_CHAR();
        if (c == ']' && GET_DEPTH() == 0)
            goto end;
        else if (c == '=' && GET_DEPTH() > 0) {
            int count = 1;
            while ((c = GET_NEXT_CHAR()) == '=')
                count++;
            if (c == ']' && count == GET_DEPTH())
                goto end;
            else {
                ADD_BYTE_TO_BUF(']');
                while (count-- > 0)
                    ADD_BYTE_TO_BUF('=');
                unput(c);
            }
        }

        break;
end:
        LVAL_STRING = DUMP_CURR_BUF();
        RESET_CURR_BUF();
        BEGIN(INITIAL);
        return TK_STRING;
    }while(0);}
}


<squote_string>{
    '	{
        LVAL_STRING = DUMP_CURR_BUF();
        RESET_CURR_BUF();
        BEGIN(INITIAL);
        return TK_STRING;
    }
    (''|[^\\'\n])+	ADD_STRING_TO_BUF();
}

<dquote_string>{
    \"	{
        BEGIN(INITIAL);
    LVAL_STRING = DUMP_CURR_BUF();
    RESET_CURR_BUF();
    return TK_STRING;
}
(\"\"|[^\\\"\n])+	ADD_STRING_TO_BUF();
}

<squote_string,dquote_string>{
    \\	{do{
        int c = GET_NEXT_CHAR();
        if (!isdigit(c)) {
            switch (c) {
                case 'a': ADD_BYTE_TO_BUF('\a'); break;
                case 'b': ADD_BYTE_TO_BUF('\b'); break;
                case 't': ADD_BYTE_TO_BUF('\t'); break;
                case 'n': ADD_BYTE_TO_BUF('\n'); break;
                case 'f': ADD_BYTE_TO_BUF('\f'); break;
                case 'r': ADD_BYTE_TO_BUF('\r'); break;
                case 'v': ADD_BYTE_TO_BUF('\v'); break;
                default:  ADD_BYTE_TO_BUF(c); break;
            }
            break;
        }
        if (c == '0')
            c = GET_NEXT_CHAR();
        if (!isdigit(c)) {
            SET_ERRMSG(MSGL_INVALID_BYTE_SEQUENCE);
            return -1;
        } else {
            int d = GET_NEXT_CHAR();
            if (!isdigit(d)) {
                SET_ERRMSG(MSGL_INVALID_BYTE_SEQUENCE);
                return -1;
            }
            char buf[3] = { (char)c, (char)d, '\0'};
            ADD_BYTE_TO_BUF(atoi(buf));
        }
    }while(0);}	
    \n	{
        INCREASE_LINENO();
        SET_ERRMSG(MSGL_UNCLOSED_STRING);
        BEGIN(INITIAL);
        return -1;
    }
}


    /* lua 中的数字:
     ** 小数点是可选的，指数部分也是可选的，支持16进制，例如：
     **	3 3.0 3.1416 314.15e-2 0.31416E1 0xff 0x56
     */
{HEX}		LVAL_NUMBER = strtol(yytext + 2, NULL, 16); return TK_NUMBER;
{NUMBER}	LVAL_NUMBER = atof(yytext); return TK_NUMBER;


"+"	 return TK_PLUS;
"-"	 return TK_MINUS;
"*"	 return TK_TIMES;
\/	 return TK_DIVIDE;
"%"	 return TK_MOD;
"#"	 return TK_POUND;
"<"	return TK_LT;
">"	return TK_GT;
"="	return TK_ASSIGN;
"("	return TK_LEFT_PARENTHESIS;
")"	return TK_RIGHT_PARENTHESIS;
"{"	return TK_LEFT_BRACE;
"}"	return TK_RIGHT_BRACE;
"]"	return TK_RIGHT_BRACKET;
";"	return TK_SEMICOLON;
":"	return TK_COLON;
","	return TK_COMMA;
"."	return TK_PERIOD;
"^"     return TK_EXP;

    /* long operator */
"=="	{ return TK_EQ;}
"~="	{ return TK_NE; }
"<="	{ return TK_LE; }
">="	{ return TK_GE; }
".."	return TK_CONCAT;
"..."	return TK_VAR_ARG;

    /* reserve word */
break		{ return TK_BREAK; }
and		{ return TK_AND; }
do		{ return TK_DO; }
else		{ return TK_ELSE; }
elseif		{ return TK_ELSEIF; }
end		{ return TK_END; }
"false"		{ return TK_FALSE; }
for		{ return TK_FOR; }
function	{ return TK_FUNCTION; }
if		{ return TK_IF; }
in		{ return TK_IN; }
local		{ return TK_LOCAL; }
nil		{ return TK_NIL; }
not		{ return TK_NOT; }
or		{ return TK_OR; }
repeat		{ return TK_REPEAT; }
return		{ return TK_RETURN; }
then		{ return TK_THEN; }
"true"		{ return TK_TRUE; }
until		{ return TK_UNTIL; }
while		{ return TK_WHILE; }


{NAME}	{
         LVAL_NAME = tinaC_retrieve(LITERAL_POOL, yytext);
         return TK_NAME;
        }



    /* fallback rules */
[ \t]+	/* eat up blank */
\n	INCREASE_LINENO();
.	{
    SET_ERRMSG(MSGL_UNKNOWN_TOKEN);
    return -1;
}

%%

static FILE *lexer_tracer_stream = NULL;
static const char *lexer_tracer_prefix = NULL;
#ifndef NDEBUG
static void lexer_tracer(LexerState *s, int token)
{
    switch (token) {
    case TK_STRING:
        fprintf(lexer_tracer_stream,  "%s\tSTRING:\t[[%s]]\n", lexer_tracer_prefix, s->lval.string);
        break;
    case TK_NAME:
        fprintf(lexer_tracer_stream,  "%s\tNAME:\t%s\n", lexer_tracer_prefix, s->lval.name);
        break;
    case TK_NUMBER:
        fprintf(lexer_tracer_stream,  "%s\tNUMER:\t%f\n", lexer_tracer_prefix, s->lval.number);
        break;
    case TK_AND:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tand\n", lexer_tracer_prefix);
        break;
    case TK_END:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tend\n", lexer_tracer_prefix);
        break;
    case TK_IN:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tin\n", lexer_tracer_prefix);
        break;
    case TK_REPEAT:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\trepeat", lexer_tracer_prefix);
        break;
    case TK_BREAK:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tbreak\n", lexer_tracer_prefix);
        break;
    case TK_FALSE:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tfalse\n", lexer_tracer_prefix);
        break;
    case TK_LOCAL:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tlocal\n", lexer_tracer_prefix);
        break;
    case TK_RETURN:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\treturn\n", lexer_tracer_prefix);
        break;
    case TK_DO:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tdo\n", lexer_tracer_prefix);
        break;
    case TK_FOR:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tfor\n", lexer_tracer_prefix);
        break;
    case TK_NIL:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tnil\n", lexer_tracer_prefix);
        break;
    case TK_THEN:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tthen\n", lexer_tracer_prefix);
        break;
    case TK_ELSE:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\telse\n", lexer_tracer_prefix);
        break;
    case TK_FUNCTION:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tfunction\n", lexer_tracer_prefix);
        break;
    case TK_NOT:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tnot\n", lexer_tracer_prefix);
        break;
    case TK_TRUE:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\ttrue\n", lexer_tracer_prefix);
        break;
    case TK_ELSEIF:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\telseif\n", lexer_tracer_prefix);
        break;
    case TK_IF:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tif\n", lexer_tracer_prefix);
        break;
    case TK_OR:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tor\n", lexer_tracer_prefix);
        break;
    case TK_UNTIL:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\tuntil\n", lexer_tracer_prefix);
        break;
    case TK_WHILE:
        fprintf(lexer_tracer_stream,  "%s\tReserve Word:\twhile\n", lexer_tracer_prefix);
        break;
    case TK_EQ:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "==");
        break;
    case TK_NE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "~=");
        break;
    case TK_LE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "<=");
        break;
    case TK_GE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ">=");
        break;
    case TK_CONCAT:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "..");
        break;
    case TK_VAR_ARG:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "...");
        break;
    case TK_PLUS:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "+");
        break;
    case TK_MINUS:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "-");
        break;
    case TK_TIMES:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "*");
        break;
    case TK_DIVIDE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "/");
        break;
    case TK_MOD:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "%");
        break;
    case TK_EXP:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "^");
        break;
    case TK_POUND:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "#");
        break;
    case TK_LT:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "<");
        break;
    case TK_GT:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ">");
        break;
    case TK_ASSIGN:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "=");
        break;
    case TK_RIGHT_PARENTHESIS:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ")");
        break;
    case TK_LEFT_PARENTHESIS:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "(");
        break;
    case TK_LEFT_BRACE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "{");
        break;
    case TK_RIGHT_BRACE:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "}");
        break;
    case TK_LEFT_BRACKET:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "[");
        break;
    case TK_RIGHT_BRACKET:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, "]");
        break;
    case TK_SEMICOLON:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ";");
        break;
    case TK_COLON:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ":");
        break;
    case TK_COMMA:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ",");
        break;
    case TK_PERIOD:
        fprintf(lexer_tracer_stream,  "%s\tOperator:\t'%s'\n", lexer_tracer_prefix, ".");
        break;
    }
}
#   undef MYOUTPUT_BUFSIZE
void LexerTrace(FILE *TraceFILE, const char *prefix)
{
    lexer_tracer_stream = TraceFILE;
    lexer_tracer_prefix = prefix;
}
#else
void LexerTrace(FILE *TraceFILE, const char *prefix)
{
    /* empty stub */
}
#endif

static TINA_PROC_MSG(default_error_function)
{
    FILE *stream = (FILE*)opaque;
    fprintf(stream, "%s\n", msg);
}

void tinaL_init(LexerState *s)
{
    s->line_number = 1;
    s->string_buffer = NULL;
    s->constant_pool = NULL;
    s->scanner = NULL;
    tinaL_set_error_func(s, stderr, default_error_function);
}

void tinaL_set_file_source(LexerState *s, void *filesrc)
{
    yylex_init_extra(s, &s->scanner);
    yyset_in((FILE*)filesrc, s->scanner);
}

void tinaL_set_string_source(LexerState *s, const char *stringsrc)
{
    YY_BUFFER_STATE new_state;
    yylex_init_extra(s, &s->scanner);
    new_state = yy_scan_string(stringsrc, s->scanner);
    yy_switch_to_buffer(new_state, s->scanner);
}

void tinaL_set_constant_pool(LexerState *s, tina_ConstPool *cp)
{
    s->constant_pool = cp;
}
void tinaL_set_string_buffer(LexerState *s, tinaU_String  *sb)
{
    s->string_buffer = sb;
}

void tinaL_free(LexerState *s)
{
    yylex_destroy(s->scanner);
}

int tinaL_scan(LexerState *s, tinaA_Token **token)
{
    int id = yylex(s->scanner);
#ifndef NDEBUG
    if (lexer_tracer_stream != NULL) {
        lexer_tracer(s, id);
    }
#endif
    switch (id) {
    case TK_NAME:
    case TK_STRING:
    case TK_NUMBER:
        *token = NEW(*token);
        (*token)->type = id;
        if (id == TK_NAME) {
            (*token)->body.name = s->lval.name;
        } else if (id == TK_STRING) {
            (*token)->body.string = s->lval.string;
        } else {
            (*token)->body.number = s->lval.number;
        }
        break;
    default:
        *token = NULL;
        break;
    }
    return id;
}

/* set error/warning display callback */
void tinaL_set_error_func(LexerState *s
    , void *error_opaque, tina_MsgFunc error_func)
{
        s->error_func = error_func;
        s->error_opaque = error_opaque;
}
