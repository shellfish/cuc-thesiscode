#include <stdlib.h>

#include "tast.h"
#include "tina.h"

void Ast_release_setlist(struct AstNode_setlist *s)
{
    struct AstNode_setlist *p = s, *next;
    while (1) {
        next = NEXT(p);
        Ast_release_varlist(p->varlist);
        FREE(p);
        if (next == s)
            break;
        p = next;
    }
}

void Ast_release_statlist(struct AstNode_statlist *s)
{
    if (!s)
        return;
    struct AstNode_statlist *p;
    struct AstNode_statlist *next;

    p = s;
    while (1) {
        next = NEXT(p);

        switch (p->type) {
        case STAT_DECL:
            do {
                struct AstNode_stat_decl *s = (__typeof__(s)) p;
                Ast_release_namelist(s->namelist);
                Ast_release_explist(s->explist);
            } while (0);
            break;
        case STAT_DECL_FUNC:
            do {
                struct AstNode_stat_decl_function *s = (__typeof__(s)) p;
                Ast_release_funcbody(s->funcbody);
            } while (0);
            break;
        case STAT_DOBLOCK:
            do {
                struct AstNode_stat_doblock *s = (__typeof__(s)) p;
                Ast_release_statlist(s->statlist);
            } while (0);
            break;
        case STAT_WHILE:
            do {
                struct AstNode_stat_while *s = (__typeof__(s)) p;
                Ast_release_exp(s->exp);
                Ast_release_statlist(s->statlist);
            } while (0);
            break;
        case STAT_ENUM_FOR:
            do {
                struct AstNode_stat_enum_for *s = (__typeof__(s)) p;
                Ast_release_statlist(s->statlist);
                Ast_release_explist(s->explist);
            } while (0);
            break;
        case STAT_GENERIC_FOR:
            do {
                struct AstNode_stat_generic_for *s = (__typeof__(s)) p;
                Ast_release_namelist(s->namelist);
                Ast_release_explist(s->explist);
                Ast_release_statlist(s->statlist);
            } while (0);
            break;
        case STAT_REPEAT:
            do {
                struct AstNode_stat_repeat *s = (__typeof__(s)) p;
                Ast_release_exp(s->exp);
                Ast_release_statlist(s->statlist);
            } while (0);
            break;
        case STAT_COND:
            do {
                struct AstNode_stat_cond *s = (__typeof__(s)) p;
                Ast_release_condlist(s->condlist);
            } while (0);
            break;
        case STAT_FUNC:
            do {
                struct AstNode_stat_func *s = (__typeof__(s)) p;
                Ast_release_namelist(s->funcname);
                Ast_release_funcbody(s->funcbody);
            } while (0);
            break;
        case STAT_RETURN:
            do {
                struct AstNode_stat_leave *s = (__typeof__(s)) p;
                Ast_release_explist(s->explist);
            } while (0);
            break;
        case STAT_BREAK:
            break;
        case STAT_ASSIGN:
            do {
                struct AstNode_stat_assign *s = (__typeof__(s)) p;
                Ast_release_setlist(s->setlist);
                Ast_release_explist(s->explist);
            } while (0);
            break;
        case STAT_FUNCALL:
            do {
                struct AstNode_stat_funcall *s = (__typeof__(s)) p;
                Ast_release_varlist(s->funcall);
            } while (0);
            break;
        default:
            assert(0);
            abort();
        }
        FREE(p);
        if (next == s)
            break;
        p = next;
    }
}

void Ast_release_exp(struct AstNode_exp *e)
{
    if (!e)
        return;
    switch (e->type) {
    case EXP_PREFIX:
        do {
            struct AstNode_exp_prefix *p = (__typeof__(p)) e;
            Ast_release_varlist(p->prefixexp);
        } while (0);
        break;
    case EXP_TABLECONSTRUCTOR:
        do {
            struct AstNode_exp_tableconstructor *p = (__typeof__(p)) e;
            Ast_release_fieldlist(p->fieldlist);
        } while (0);
        break;
    case EXP_FUNCTION:
        do {
            struct AstNode_exp_function *p = (__typeof__(p)) e;
            Ast_release_funcbody(p->function);
        } while (0);
        break;
    case EXP_UNARY_NOT:
    case EXP_UNARY_MINUS:
    case EXP_UNARY_POUND:
        do {
            struct AstNode_exp_unary *p = (__typeof__(p)) e;
            Ast_release_exp(p->exp);
        } while (0);
        break;
    case EXP_OR:
    case EXP_AND:
    case EXP_LT:
    case EXP_GT:
    case EXP_LE:
    case EXP_GE:
    case EXP_EQ:
    case EXP_NE:
    case EXP_CONCAT:
    case EXP_PLUS:
    case EXP_MINUS:
    case EXP_TIMES:
    case EXP_DIVIDE:
    case EXP_MOD:
    case EXP_EXPONENT:
        do {
            struct AstNode_exp_binary *p = (__typeof__(p)) e;
            Ast_release_exp(p->exp[0]);
            Ast_release_exp(p->exp[1]);
        } while (0);
        break;
    case EXP_STRING:
        do {
            struct AstNode_exp_string *p = (__typeof__(p)) e;
            FREE((void *) p->string);
        } while (0);
        break;
    case EXP_NIL:
    case EXP_TRUE:
    case EXP_FALSE:
    case EXP_VARARG:
    case EXP_NUMBER:
        /* do nothging  */
        break;
    }
    FREE(e);
}

void Ast_release_explist(struct AstNode_explist *e)
{
    if (!e)
        return;
    struct AstNode_explist *p;
    struct AstNode_explist *next;

    p = e;
    while (1) {
        next = NEXT(p);
        Ast_release_exp(p->exp);
        FREE(p);
        if (next == e)
            break;
        p = next;
    }
}

void Ast_release_varlist(struct AstNode_varlist *v)
{
    if (!v)
        return;
    struct AstNode_varlist *p;
    struct AstNode_varlist *next;

    p = v;
    while (1) {
        next = NEXT(p);
        switch (p->type) {
        case VAR_NAME:
            /*  do nothging */
            break;
        case VAR_PREFIXEXP:
            do {
                struct AstNode_var_prefixexp *v = (__typeof__(v)) p;
                Ast_release_exp(v->exp);
            } while (0);
            break;
        case VAR_FUNCALL_EMPTY:
            /* do nothing  */
            break;
        case VAR_FUNCALL_EXP:
            do {
                struct AstNode_var_funcall_exp *v = (__typeof__(v)) p;
                Ast_release_explist(v->explist);
            } while (0);
            break;
        case VAR_FUNCALL_TABLE:
            do {
                struct AstNode_var_funcall_table *v = (__typeof__(v)) p;
                Ast_release_fieldlist(v->fieldlist);
            } while (0);
            break;
        case VAR_FUNCALL_STRING:
            do {
                struct AstNode_var_funcall_string *v = (__typeof__(v)) p;
                FREE((void *) v->string);
            } while (0);
            break;
        case VAR_SUBSCRIPT:
            do {
                struct AstNode_var_subscript *v = (__typeof__(v)) p;
                Ast_release_exp(v->exp);
            } while (0);
            break;
        }
        FREE(p);
        if (next == v)
            break;
        p = next;
    }
}

void Ast_release_condlist(struct AstNode_condlist *condlist)
{
    if (!condlist)
        return;
    struct AstNode_condlist *curr;
    struct AstNode_condlist *next;

    curr = condlist;
    while (1) {
        next = NEXT(curr);
        Ast_release_exp(curr->exp);
        Ast_release_statlist(curr->statlist);
        FREE(curr);
        if (next == condlist)
            break;
        curr = next;
    }
}

void Ast_release_namelist(struct AstNode_namelist *namelist)
{
    if (!namelist)
        return;
    struct AstNode_namelist *curr, *next;
    curr = namelist;
    while (1) {
        next = NEXT(curr);
        FREE(curr);
        if (next == namelist)
            break;
        curr = next;
    }
}

void Ast_release_funcbody(struct AstNode_funcbody *funcbody)
{
    if (!funcbody)
        return;
    Ast_release_params(funcbody->params);
    Ast_release_statlist(funcbody->statlist);
    FREE(funcbody);
}

void Ast_release_params(struct AstNode_params *params)
{
    if (!params)
        return;
    Ast_release_namelist(params->namelist);
    FREE(params);
}

void Ast_release_fieldlist(struct AstNode_fieldlist *fieldlist)
{
    if (!fieldlist)
        return;
    struct AstNode_fieldlist *curr, *next;

    curr = fieldlist;
    while (1) {
        next = NEXT(curr);
        switch (curr->type) {
        case FIELD_ONLY_VALUE:
            do {
                struct AstNode_fieldlist_only_value *f = (__typeof__(f)) curr;
                Ast_release_exp(f->exp);
            } while (0);
            break;
        case FIELD_NAME_KEY:
            do {
                struct AstNode_fieldlist_name_key *f = (__typeof__(f)) curr;
                Ast_release_exp(f->exp);
            } while (0);
            break;
        case FIELD_INDEX_KEY:
            do {
                struct AstNode_fieldlist_index_key *f = (__typeof__(f)) curr;
                Ast_release_exp(f->exp);
                Ast_release_exp(f->index);
            } while (0);
            break;
        }
        FREE(curr);
        if (next == fieldlist)
            break;
        curr = next;
    }
}

void Ast_release_ublock(struct AstNode_ublock *ublock)
{
    if (ublock) {
        Ast_release_statlist(ublock->statlist);
        Ast_release_exp(ublock->exp);
        FREE(ublock);
    }
}

void Ast_release_repetition(struct AstNode_repetition *repetition)
{
    if (repetition) {
        Ast_release_explist(repetition->explist);
        Ast_release_namelist(repetition->namelist);
        FREE(repetition);
    }
}

void Ast_release_args(struct AstNode_args *args)
{
    if (!args)
        return;
    switch (args->type) {
    case ARGS_EMPTY:
        /*  do nothgng */
        break;
    case ARGS_EXPLIST:
        Ast_release_explist(((struct AstNode_args_explist *) args)->explist);
        break;
    case ARGS_TABLECONSTRUCTOR:
        Ast_release_fieldlist(((struct AstNode_args_tableconstructor *) args)->
                              fieldlist);
        break;
    case ARGS_STRING:
        FREE((void *) (((struct AstNode_args_string *) args)->string));
        break;
    }
    FREE(args);
}

void AstToken_release(struct AstToken *t)
{
    if (t) {
        if (t->type == TK_STRING)
            FREE((void *) t->body.string);
        FREE(t);
    }
}
