/* modeline: vim: set tabstop=8 softtabstop=4 shiftwidth=4 expandtab:  */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "auxlib/tmacro.h"
#include "config.h"
#include "libtina.h"

#ifndef NDEBUG
void ParseTrace(FILE *TraceFILE,  char *zTracePrompt);
#endif
void LexerTrace(FILE *TraceFILE, const char *promt);

static const char *option_string = ":hv?o:e:f:dc";
static const struct option long_options[] = {
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {"output", required_argument, NULL, 'o'},
    {"format", required_argument, NULL, 'f'},
    {"stdout", no_argument, NULL, 'c'},
    {"expr", required_argument, NULL, 'e'},
#ifndef NDEBUG
    {"debug", optional_argument, NULL, 'd'},
#endif
    {NULL, 0, NULL, 0}
};

static const char *option_usage[] = {
    "display this help and exit",
    "output version information and exit",
    "specify output filenane(default 'tinac.out')",
    "output format:'bytecode'(default) or 'graphviz'",
    "redirect output file to stdout",
    "`--expr \"string\"', read input from string",
#ifndef NDEBUG
    "`--debug=parser:lexer' will turn all tracer on",
#endif
};

static void print_help_message(const char *execname);
static const char *output_filename = "tinac.out";
static const char *output_format = "bytecode";
static char *enable_debug = NULL;
static const char *input_filename = NULL;
static char *input_string = NULL;
static FILE *input_stream = NULL;
static void (*output_func)(TinaState *s) = NULL;

typedef struct logger_t {
    TinaState *ts;
    const char *prefix;
} logger_t;

static TINA_PROC_MSG(log_proc)
{
    logger_t *p = (logger_t*)opaque;
    fprintf(stderr, "%s:%d %s\n", p->prefix
            , tina_get_line_number(p->ts), msg);
}

static TINA_PROC_OUPUT(proc_output)
{
    FILE *output_stream = (FILE*)opaque;
    fwrite(data, sz, 1, output_stream);
}

static void clean_exit(int exitcode, TinaState *s)
{
    tina_delete(s);
    exit(exitcode);
}


int main(int argc, char *argv[])
{
    int c;
    FILE *output_stream = NULL;
    int  option_index = 0;

    while((c = getopt_long(argc, argv, option_string
                    , long_options, &option_index)) != -1) {
        switch (c) {
        case 'h':
            print_help_message(argv[0]);
            exit(EXIT_SUCCESS);
        case 'v':
            printf("%s (using %s)\nCopyright (C) 2011 %s\n"
             ,argv[0], PACKAGE_STRING, PACKAGE_BUGREPORT);
            exit(EXIT_SUCCESS);
        case 'o':
            output_filename = optarg;
            break;
        case 'f':
            output_format = optarg;
            break;
        case 'd':
            enable_debug = optarg ? strdup(optarg) : strdup("lexer");
            break;
        case 'c':
            output_stream = stdout;
            break;
        case 'e':
            input_string = strdup(optarg);
            break;
        case '?': do {
                const char *p = (optind > argc - 1)
                        ? argv[optind-1]
                        : argv[optind];
                fprintf(stderr, "%s: invalid option -- '%s'\n"
                        "Try `%s --help' for more information.\n"
                        , argv[0]
                        , p[1] == '-'
                            ? p + 2  /*  long option */
                            : p + 1  /*  short option */
                        , argv[0]);
                exit(EXIT_FAILURE);
            } while (0);
        }
    }


    if (input_string == NULL) {
        if (optind == argc) { /* no FILE argument */
            input_filename = "-";
        } else if (optind <= argc - 1) {
            input_filename = argv[optind];
        } else {
            /* cannot reach here  */
            assert(0);
            abort();
        }

       if (input_filename[0] == '-' && input_filename[1] == '\0') {
            input_stream = stdin;
        } else {
            input_stream = fopen(input_filename, "r");
            if (!input_stream) {
                fprintf(stderr, "%s: %s: No such file or directory\n"
                        , argv[0], input_filename);
                exit(EXIT_FAILURE);
            }
        }
    }


    do {
        if (output_format[0] == 'b'
            && strncmp(output_format, "bytecode", 10) == 0) {
                output_func = tina_output_bytecode;
                break;
        } else if (output_format[0] == 'g'
            && strncmp(output_format, "graphviz", 10) == 0) {
                output_func = tina_output_graphviz;
                break;
        }

        fprintf(stderr, "output format not recognized\n");
        exit(EXIT_FAILURE);
    } while (0);

    TinaState *s = tina_new();
    if (input_string) {
        tina_set_string_source(s, input_string);
        free(input_string);
    } else {
        tina_set_file_source(s, input_stream);
    }

    logger_t logger = {s, argv[0]};
    tina_set_error_func(s, &logger, log_proc);

#ifndef NDEBUG
    if (enable_debug) {
        char *p = strtok(enable_debug, ":");
        int enable_lexer_tracer = 0;
        int enable_parser_tracer = 0;
        while (p) {
            if (strncmp(p, "lexer", 10) == 0) {
                enable_lexer_tracer = 1;
            } else if (strncmp(p, "parser", 10) == 0) {
                enable_parser_tracer = 1;
            } else {
                fprintf(stderr, "%s: %s: Unkown tracer name\n"
                    , argv[0], p);
                clean_exit(EXIT_FAILURE, s);
            }
            p = strtok(NULL, ":");
        }

        if (enable_lexer_tracer) {
            LexerTrace(stderr, "\e[33mLexer:\e[0m ");
        }
        if (enable_parser_tracer) {
            ParseTrace(stderr, (char*)"\e[32mParser:\e[0m ");
        }
        free(enable_debug);
    }
#endif

    int compile_result = tina_compile(s);

    if (input_stream)
        fclose(input_stream);
    if (!compile_result) {
        clean_exit(EXIT_FAILURE, s);
    }

    if (! output_stream) {
        output_stream = fopen(output_filename, "wb");
        if (!output_stream) {
            fprintf(stderr, "%s: %s: No such file or directory\n"
                    , argv[0], output_filename);
            clean_exit(EXIT_FAILURE, s);
        }
    }


    tina_set_output_func(s, output_stream, proc_output);
    output_func(s);
    fflush(output_stream);

    fclose(output_stream);
    clean_exit(EXIT_SUCCESS, s);

    return EXIT_SUCCESS;
}

void print_help_message(const char *execname)
{
    static const char *presence_help_message =
    "Usage: %s [OPTION] ... [FILE]\n"
    "compile FILE in script language Lua.\n\n";

    static const char *extra_help_message =
    "With no FILE, or when FILE is -, read standard input.\n";

    printf(presence_help_message, execname);
    int i;
    const struct option *p;
    int option_leng = ARRAYSIZE(long_options) - 1;
    for (i = 0; i < option_leng; i++) {
        p = &long_options[i];
        if (p->val == 0) {  /* has no corresponding short option  */
            printf("%5s", " ");
        } else {
            printf(" -%c, ", p->val);
        }
        printf("--%-15s%s\n",  p->name, option_usage[i]);
    }
    printf("\n%s\n", extra_help_message);
}
