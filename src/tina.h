#ifndef tina_h
#define tina_h

#include "config.h"

#include "auxlib/tmacro.h"

#include "tinadef.h"
#include "ttabs.h"
#include "tparser.h"

    /* ----------------
     |  Lexer section |
     =================*/

/* An opaque pointer. take from flex-generated source */
#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;
#endif

typedef struct LexerState {
    union {
        const char *name;
        char *string;
        double number;
    } lval;
    yyscan_t scanner;
    int line_number;
    int nesting_depth;
    tinaU_String    *string_buffer;
    tina_ConstPool  *constant_pool;
    tina_MsgFunc    error_func;
    void            *error_opaque;
} LexerState;

void tinaL_init(LexerState *s);

void tinaL_set_file_source(LexerState *s, void *filesrc);
void tinaL_set_string_source(LexerState *s, const char *strsrc);

void tinaL_set_constant_pool(LexerState *s, tina_ConstPool *cp);
void tinaL_set_string_buffer(LexerState *s, tinaU_String  *sb);

/* set error/warning display callback */
void tinaL_set_error_func(LexerState *s
        , void *error_opaque, tina_MsgFunc error_func);

void tinaL_free(LexerState *s);
int  tinaL_scan(LexerState *s, tinaA_Token **token);

    /*-------------------
     |  Parser Section  |
     ==================*/

struct ParserState;
typedef struct ParserState {
    void *parser;
    int  status;
    LexerState *lexer_state;
    void (*error_func)(void *opaque, const char *msg);
    void *error_opaque;
    tinaA_Root *root;
}ParserState;

void tinaP_init(ParserState *s);
void tinaP_set_lexer(ParserState *ps, LexerState *ls);
void tinaP_set_error_func(ParserState *s
        , void *error_opaque, tina_MsgFunc error_func);

tinaA_Root *tinaP_compile(ParserState *s);
void tinaP_free(ParserState *s);

    /*----------------------------
     | constant code and message |
     ============================*/
typedef enum tinaE_ConstCode {
    MSGL_INVALID_OPEN_BRACKET,
    MSGL_INVALID_BYTE_SEQUENCE,
    MSGL_UNCLOSED_STRING,
    MSGL_UNKNOWN_TOKEN,
    MSGP_UNHPOELESS_FAILURE,
    CONSTCODE_END,
} tinaE_ConstCode;

const char *tinaE_getstr(tinaE_ConstCode code);

void tinaG_generate_bytecode(tinaA_Root *root
                        , void *output_opaque
                        , tina_OuputFunc output_func);

void tinaA_generate_graph(tinaA_Root *root
                        , void *output_opaque
                        , tina_OuputFunc output_func);


#endif
