#include "ttabs.h"
#include "auxlib/tmacro.h"
#include "auxlib/tavl.h"

#include <string.h>
#include <stdlib.h>

#define obstack_chunk_alloc malloc
#define obstack_chunk_free  free

    /* ----------------|
     * | Constant Pool |
     * |---------------- */
static tinaU_ALLOCATOR(defult_const_pool_allocator)
{
    tina_ConstPool *cp = (tina_ConstPool*)allocator_opaque;
    return obstack_alloc(&cp->stack, sz);
}

void tinaC_init(tina_ConstPool *cp)
{
    tinaU_initAvl(&cp->tree);
    cp->tree.comparer = (tinaU_Comparer)strcmp;
    tinaU_avlSetAllocator(&cp->tree, cp, defult_const_pool_allocator);
    tinaU_avlSetReleaser(&cp->tree, NULL, NULL);
    obstack_init(&cp->stack);
}

const char *
tinaC_retrieve(tina_ConstPool *cp, const char *name)
{
    tinaU_AvlNode *n = tinaU_avlReplace(&cp->tree, name);
    if (!n->data) {
        size_t len = strlen(name) + 1;
        void *p = obstack_alloc(&cp->stack, len);
        memcpy(p, name, len);
        n->data = p;
    }
    return (const char*)n->data;
}

void  tinaC_free(tina_ConstPool *cp)
{
    tinaU_freeAvl(&cp->tree);
    obstack_free(&cp->stack, NULL);
}

    /* ---------------|
     * | Underlay Map |
     * |--------------- */
//typedef struct Group {
//    const char *name;
//    int   register_id;
//} Group;
//
//static tinaU_COMPARER(map_comparer)
//{
//    const Group *c = (const Group*)dest;
//    const Group *d = (const Group*)src;
//    return c->name == d->name ? 0 : strcmp(c->name, d->name);
//}
//
//static void map_node_initializer(void *pdata)
//{
//    Group *p = NEW(p);
//    p->register_id = -1;
//    *((Group**)pdata) = p;
//}
//
//Map  *map_new(void)
//{
//    Map *t = NEW(t);
//    tinaU_initAvl();
//    t->comparer = map_comparer;
//    t->node_initializer = map_node_initializer;
//    return t;
//}
//
//void map_delete(Map *m)
//{
//    tinaU_destroyAvl(m);
//}
//
//int   map_query(Map *m, const char *name, int candidate_register_id)
//{
//    Group g;
//    g.name = name;
//    tinaU_AvlNode *n = tinaU_avlReplace(m, &g);
//    Group *pg = (Group*)n->data;
//    if (pg->register_id == -1) {
//        /* is new node */
//        pg->register_id = candidate_register_id;
//    }
//    return pg->register_id;
//}
