#ifndef libtina_h
#define libtina_h


#include <stddef.h>
#include "tinadef.h"

struct TinaState;
typedef struct TinaState TinaState;

/* create a new Tina compilation context */
TinaState *tina_new(void);

/* free a Tina compilation context */
void  tina_delete(TinaState *s);

/* set error/warning display callback */
void  tina_set_error_func(TinaState *s
        , void *error_opaque, tina_MsgFunc error_func);

/* set output callback */
void  tina_set_output_func(TinaState *s
       , void *output_opaque, tina_OuputFunc output_func);

void  tina_set_file_source(TinaState *s, void *file_stream);
void  tina_set_string_source(TinaState *s, const char *string);


int   tina_get_line_number(TinaState *s);

int   tina_compile(TinaState *s);
void  tina_output_bytecode(TinaState *s);
void  tina_output_graphviz(TinaState *s);

#endif
