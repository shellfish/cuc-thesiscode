#ifndef ttabs_h
#define ttabs_h

#include <obstack.h>
#include "tinadef.h"
#include "auxlib/tavl.h"

typedef struct tina_ConstPool tina_ConstPool;
struct tina_ConstPool {
    struct obstack stack;
    tinaU_AvlTree  tree;
};

tina_ConstPool *tinaC_new(void);

void        tinaC_init(tina_ConstPool *cp);
const char *tinaC_retrieve(tina_ConstPool *cp, const char *name);
void        tinaC_free(tina_ConstPool *cp);


/* bind implenmentation of Map to avltree  */
typedef tinaU_AvlTree Map;

void map_init(Map *m);
void map_free(Map *m);
int  map_query_or_create(const char *name, int cndidate_register_id);

typedef struct SymFrame SymFrame;
struct SymFrame {
    Map         map;
    SymFrame    *prev;
};

typedef struct SymTab {
    struct obstack stack;
    int    max_register_id;     /* start from 0 */
    SymFrame *curr_frame;
} SymTab;

void symtab_init(SymTab *st);
void symtab_push_frame(SymTab *st);
void symtab_pop_frame(SymTab  *st);
int  symtab_query(const char *name);
void symtab_free(SymTab *st);

#endif
