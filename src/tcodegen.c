#include "tina.h"
#include "tast.h"

#include "auxlib/list.h"

#include <stdint.h>
#include <stdlib.h>
#include "obstack.h"

#ifdef USE_INTEGRAL_NUMBER_TYPE
#define LUA_NUMBER int
#else
#define LUA_NUMBER double
#endif

#define WRITER_CLOSURE output_opaque, output_func
#define WRITER_CLOSURE_PROTO void *output_opaque\
, tina_OuputFunc output_func

typedef enum ConstantEnum {
    LUA_TNIL = 0,
    LUA_TBOOLEAN = 1,
    LUA_TNUMBER = 3,
    LUA_TSTRING = 4
} ConstantEnum;

typedef enum VarargEnum {
    VARARG_HASARG = 1,
    VARARG_ISVARARG = 2,
    VARARG_NEEDSARG = 4
} VarargEnum;

typedef uint8_t Byte;
typedef uint32_t Instruction;

typedef struct String {
    size_t size;          /* include tail 'NUL' charactor  */
    Byte   data[FLEXIBLE_ARRAY_MEMBER];
} String;

typedef struct InstructionList {
    int         size;
    Instruction *list;
} InstructionList;

typedef struct Constant {
    ConstantEnum type;
    union {
        int             boolval;
        LUA_NUMBER      number;
        String          string;
    } data;
} Constant;

typedef struct ConstantList {
    int size;
    Constant *list;
} ConstantList;

typedef struct Function Function;
typedef struct FunctionList {
    int size;
    Function *list;
} FunctionList;

struct Function {
    const char *source_name;
    int     line_defined;
    int     last_line_defined;
    Byte    upvalue_number;
    Byte    parameter_number;
    Byte    vararg_flag;
    Byte    max_stack_size;
    InstructionList instruction_list;
    ConstantList    constant_list;
    FunctionList    function_list;
};

typedef struct ChunkHeader {
    Byte header_signature[4];  /* 0x1B4C7561  */
    Byte version_number;       /* for lua5.1, is 0x51  */
    Byte format_version;       /* 0 = offical version  */
    Byte endianness_flag;      /* 0=big endian, 1=little endian  */
    Byte int_size;
    Byte size_t_size;
    Byte instruction_size;
    Byte luanumber_size;
    Byte integral_flag;        /* 0=float-point 1=integral  */
} ChunkHeader;

static void dump_chunk_header(tinaA_Root *root, WRITER_CLOSURE_PROTO);
static void dump_chunk(tinaA_Root *root, WRITER_CLOSURE_PROTO);

static Function *func_new(tinaA_Root *root);
static Function *func_delete(Function *fb);
static void      func_dump(Function *fb, WRITER_CLOSURE_PROTO);

void tinaG_generate_bytecode(tinaA_Root *root
                        , void *output_opaque
                        , tina_OuputFunc output_func)
{
    dump_chunk_header(root, WRITER_CLOSURE);
    //dump_chunk(root, WRITER_CLOSURE);
}

void dump_chunk_header(tinaA_Root *root, WRITER_CLOSURE_PROTO)
{
    ChunkHeader header = {{0x1b, 0x4c, 0x75, 0x61}}; /* "\eLua"  */
    header.version_number = 0x51;
    header.format_version = 0;
#ifdef WORDS_BIGENDIAN
    header.endianness_flag = 0;
#else
    header.endianness_flag = 1;  /* assume little endian, it may be wrong */
#endif
    header.int_size = sizeof(int);
    header.size_t_size = sizeof(size_t);
    header.instruction_size = 4;  /* Lua uses 4byte-leng instruction */
    header.luanumber_size = sizeof(LUA_NUMBER);
#ifdef USE_INTEGRAL_NUMBER_TYPE
    header.integral_flag = 1;
#else
    header.integral_flag = 0;
#endif
    output_func(output_opaque, &header, sizeof(header));
}

void dump_chunk(tinaA_Root *root, WRITER_CLOSURE_PROTO)
{
    Function *root_func = func_new(root);

    /* specific of top-level function prototype  */
    root_func->source_name = "input";
    root_func->line_defined = 0;
    root_func->parameter_number = 0;
    root_func->vararg_flag = VARARG_ISVARARG;

    func_dump(root_func, WRITER_CLOSURE);
    func_delete(root_func);
}

Function *func_new(tinaA_Root *root)
{
    // 遍历每条语句
    //
    // 每当发现下列语句时，则进入新scope
    //  STAT_DOBLOCK
    //  STAT_WHILE
    //  STAT_ENUM_FOR
    //  STAT_GENERIC_FOR
    //  STAT_REPEAT
    //  STAT_COND
    //
    //  当遇到下列语句时，定义新的函数，并绑定变量
    //      STAT_DECL_FUNC (local)
    //      STAT_FUNC      (global or local)
    //
    //  当遇到下列函数时，绑定变量
    //      STAT_DECL
    //      STAT_ASSIGN
    //
    //  当遇到下列语句时，直接生成代码
    //      STAT_RETURN
    //      STAT_BREAK
    //      STAT_ASSIGN
    //      STAT_FUNCALL
    
    // 初始化符号表
//    struct obstack stack;
  //  obstack_init(&stack);
    ////SymTab *symtab = obstack_alloc(&stack,  sizeof());
    return NULL;
}

Function *func_delete(Function *fb)
{
    return NULL;
}

void  func_dump(Function *fb, WRITER_CLOSURE_PROTO)
{
    
}
