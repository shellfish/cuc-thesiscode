#ifndef AST_INCLUDE
#define AST_INCLUDE

#include <stddef.h>
#include "auxlib/list.h"

/*
 *  namelist : const char *
 *  fieldlist: struct AstNode_field_*
 *  statlist:  struct AstNode_stat_*
 *  explist:   struct AstNode_exp_*
 *  condlist:	struct AstNode_cond  // homogeneous
 *  varlist:	struct AstNode_var_*
 *-----------------------------------------------------------------
 * ## namelist
 *  *) parlist: maybe empty list, maybe only NULL(var arg)
 *		maybe namelist + NULL(namelist + vararg)
 *
 *
 *-----------------------------------------------------------------
 *  chunk ::= statlist
 *  statlist ::= stat1 stat2 stat3 ...
 *  stat ::= statlist.  // DO statlist END  //  STAT_DOBLOCK
 *
 *	  // WHILE exp DO statlist END
 *  stat:::=  exp statlist   // STAT_WHILE
 *	  // FOR name = exp DO statlist END
 *  stat ::= name exp statlist // STAT_ENUM_FOR
 *	// FOR namelist IN explist DO statlist END
 *  stat ::= namelist explist statlist // STAT_GENERIC_FOR
 *	// REPEAT statlist UNTIL exp
 *  stat ::= statlist exp         // STAT_REPEAT
 *	// cond1(if clause) cond2 cond3 ... cond_last(else clause)
 *  stat ::= cond1 cond2 ...       // STAT_COND
 *	// FUNCTION namelist funcbody
 *  stat ::= namelist funcbody   // STAT_FUNC
 *	// varlist ASSIGN explist
 *  stat ::= varlist explist   // STAT_ASSIGN
 *  stat ::= funcall		// STAT_FUNCALL
 *  stat ::= .	// STAT_BREAK
 *  stat ::= .  // STAT_RETURN
 *  stat ::= explist // STAT_RETURN
 *  stat ::= namelist (explist) // STAT_DECL
 *  stat ::= name funcbody // STAT_DECL_FUNC
 * */

/* gnneric link list  */
struct AstNode_namelist {
    const char *name;
    struct list link;
};

struct AstNode_explist {
    struct AstNode_exp *exp;
    struct list link;
};

 /* Level 1 -> stat */

#define CREATE_STAT(subtype, name, fallback, type_flag) \
	struct AstNode_stat_##subtype *name = NEW(name);\
	list_init(&(name)->link); \
	fallback = (struct AstNode_statlist *)(name);\
	fallback->type = type_flag;

enum Ast_STAT {
    STAT_DECL,                  /*  local name; */
    STAT_DECL_FUNC,             /*  local name = value */
    STAT_DOBLOCK,               /*  do ... end */
    STAT_WHILE,                 /*  while exp ... end */
    STAT_ENUM_FOR,              /* for i = a,b,c do ... end   */
    STAT_GENERIC_FOR,           /* for i=iter() do ... end */
    STAT_REPEAT,                /* repeat block until exp  */
    STAT_COND,                  /* if ..[elseif] .. end  */
    STAT_FUNC,                  /* function funcname ... end */
    STAT_RETURN,                /* can hold one exp  */
    STAT_BREAK,
    STAT_ASSIGN,                /* namelist = explist  */
    STAT_FUNCALL,               /*  func()  */
};

/* super class  */
struct AstNode_statlist {
    enum Ast_STAT type;
    struct list link;
};

struct AstNode_stat_leave {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_explist *explist;
};

struct AstNode_stat_repeat {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_statlist *statlist;
    struct AstNode_exp *exp;
};

struct AstNode_stat_decl {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_namelist *namelist;
    struct AstNode_explist *explist;
};

struct AstNode_stat_decl_function {
    enum Ast_STAT type;
    struct list link;
    const char *name;
    struct AstNode_funcbody *funcbody;
};

struct AstNode_stat_doblock {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_statlist *statlist;
};

struct AstNode_stat_while {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_exp *exp;
    struct AstNode_statlist *statlist;
};

struct AstNode_stat_generic_for {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_namelist *namelist;
    struct AstNode_explist *explist;
    struct AstNode_statlist *statlist;
};

struct AstNode_stat_enum_for {
    enum Ast_STAT type;
    struct list link;
    const char *name;
    struct AstNode_explist *explist;
    struct AstNode_statlist *statlist;
};

struct AstNode_stat_cond {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_condlist *condlist;
};

/* indeed, after execute this statement, we get an function variable,*/
struct AstNode_stat_func {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_namelist *funcname;  /* function name */
    struct AstNode_funcbody *funcbody;
};

struct AstNode_stat_assign {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_setlist *setlist;
    struct AstNode_explist *explist;
};

struct AstNode_stat_funcall {
    enum Ast_STAT type;
    struct list link;
    struct AstNode_varlist *funcall;    /* var list   */
};

 /* Level 2 -> exp */
enum Ast_EXP {
    /* only type  */
    EXP_NIL,
    EXP_TRUE,
    EXP_FALSE,
    EXP_VARARG,

    /* with 1 entry  */
    EXP_NUMBER,
    EXP_STRING,
    EXP_PREFIX,                 /* prefixexp */
    EXP_TABLECONSTRUCTOR,       /* with one fieldlist */
    EXP_UNARY_NOT,              /* unray operator "not"  */
    EXP_UNARY_POUND,            /* unary operator '#'  */
    EXP_UNARY_MINUS,            /* unray operator '-'  */

    /* with 2 entry  */
    EXP_FUNCTION,               /* prameters, body  */
    EXP_OR,                     /*  binary logical operator */
    EXP_AND,
    EXP_LT,                     /* <  *//* binary relation operator  */
    EXP_GT,                     /* >  */
    EXP_LE,                     /* <=  */
    EXP_GE,                     /* >=  */
    EXP_EQ,                     /*  == */
    EXP_NE,                     /* ~=  */
    EXP_CONCAT,                 /*  ..  */
    EXP_PLUS,                   /* +  */
    EXP_MINUS,                  /* - */
    EXP_TIMES,                  /* * */
    EXP_DIVIDE,                 /* / */
    EXP_MOD,                    /* %  */
    EXP_EXPONENT,               /* ^  */
};

#define CREATE_EXP(subtype, name, fallback, type_flag) \
	struct AstNode_exp_##subtype *name = NEW(name);\
	fallback = (struct AstNode_exp *)(name);\
	fallback->type = type_flag;

struct AstNode_exp {
    enum Ast_EXP type;
};

struct AstNode_exp_number {
    enum Ast_EXP type;
    double number;
};

struct AstNode_exp_string {
    enum Ast_EXP type;
    const char *string;
};

struct AstNode_exp_prefix {
    enum Ast_EXP type;
    struct AstNode_varlist *prefixexp;
};

struct AstNode_exp_tableconstructor {
    enum Ast_EXP type;
    struct AstNode_fieldlist *fieldlist;
};

struct AstNode_exp_function {
    enum Ast_EXP type;
    struct AstNode_funcbody *function;
};

struct AstNode_exp_unary {
    enum Ast_EXP type;
    struct AstNode_exp *exp;
};

struct AstNode_exp_binary {
    enum Ast_EXP type;
    struct AstNode_exp *exp[2];
};

/* field list  */
enum Ast_FIELD {
    FIELD_ONLY_VALUE,           /* {a,b,c }  */
    FIELD_NAME_KEY,             /*  { a = 12 } */
    FIELD_INDEX_KEY,            /* { [12] = 12}  */
};
#define CREATE_FIELD(subtype, name, fallback, type_flag)\
	struct AstNode_fieldlist_##subtype *name = NEW(name);\
	fallback  = (struct AstNode_fieldlist*)name;\
	name->type = type_flag;\
	list_init(&name->link);

struct AstNode_fieldlist {
    enum Ast_FIELD type;
    struct list link;
};

struct AstNode_fieldlist_only_value {
    enum Ast_FIELD type;
    struct list link;
    struct AstNode_exp *exp;
};

struct AstNode_fieldlist_name_key {
    enum Ast_FIELD type;
    struct list link;
    struct AstNode_exp *exp;
    const char *name;
};

struct AstNode_fieldlist_index_key {
    enum Ast_FIELD type;
    struct list link;
    struct AstNode_exp *exp;
    struct AstNode_exp *index;
};

struct AstNode_params {
    struct AstNode_namelist *namelist;
    int vararg;
};

struct AstNode_funcbody {
    struct AstNode_params *params;
    struct AstNode_statlist *statlist;
};

/* include var and prefixexp  */
enum Ast_VAR {
    VAR_NAME,
    VAR_SUBSCRIPT,
    VAR_PREFIXEXP,
    VAR_FUNCALL_EMPTY,
    VAR_FUNCALL_EXP,
    VAR_FUNCALL_TABLE,
    VAR_FUNCALL_STRING,
};

#define CREATE_VAR(subtype, name, fallback, type_flag) \
	struct AstNode_var_##subtype *name = NEW(name);\
	list_init(&(name)->link);\
	name->type = type_flag; \
	fallback = (struct AstNode_varlist*)(name);

struct AstNode_varlist {
    enum Ast_VAR type;
    struct list link;
};

struct AstNode_var_name {
    enum Ast_VAR type;
    struct list link;
    const char *name;
};

struct AstNode_var_subscript {
    enum Ast_VAR type;
    struct list link;
    struct AstNode_exp *exp;
};

struct AstNode_var_prefixexp {
    enum Ast_VAR type;
    struct list link;
    struct AstNode_exp *exp;
};

struct AstNode_var_funcall_exp {
    enum Ast_VAR type;
    struct list link;
    const char *colon_name;
    struct AstNode_explist *explist;    /* args  */
};

struct AstNode_var_funcall_table {
    enum Ast_VAR type;
    struct list link;
    const char *colon_name;
    struct AstNode_fieldlist *fieldlist;    /* args  */
};

struct AstNode_var_funcall_string {
    enum Ast_VAR type;
    struct list link;
    const char *colon_name;
    const char *string;
};

/* intermediate objects, will't exists in the final ast */

enum Ast_ARGS {
    ARGS_EMPTY,
    ARGS_EXPLIST,
    ARGS_TABLECONSTRUCTOR,
    ARGS_STRING,
};
#define CREATE_ARGS(subtype, name, fallback, type_flag) \
	struct AstNode_args_##subtype *name = NEW(name);\
	name->type = type_flag;\
	fallback = (struct AstNode_args*)name;

struct AstNode_args {
    enum Ast_ARGS type;
};

struct AstNode_args_explist {
    enum Ast_ARGS type;
    struct AstNode_explist *explist;
};

struct AstNode_args_tableconstructor {
    enum Ast_ARGS type;
    struct AstNode_fieldlist *fieldlist;
};

struct AstNode_args_string {
    enum Ast_ARGS type;
    const char *string;
};
struct AstNode_ublock {
    struct AstNode_statlist *statlist;
    struct AstNode_exp *exp;
};

struct AstNode_repetition {
    struct AstNode_explist *explist;
    const char *name;
    struct AstNode_namelist *namelist;
};

struct AstNode_condlist {
    struct AstNode_exp *exp;    // condition
    struct AstNode_statlist *statlist;  // code block
    struct list link;
};

struct AstNode_setlist {
    struct AstNode_varlist *varlist;
    struct list link;
};

#ifndef TINA_STRUCT_AST_ROOT
#define TINA_STRUCT_AST_ROOT
typedef struct AstNode_statlist tinaA_Root;
#endif


struct AstToken {
    int type;
    union {
        const char *name;
        const char *string;
        double number;
    } body;
};
#ifndef TINA_STRUCT_AST_TOKEN
#define TINA_STRUCT_AST_TOKEN
typedef struct AstToken tinaA_Token;
#endif


void Ast_release_setlist(struct AstNode_setlist *s);
void Ast_release_statlist(struct AstNode_statlist *s);
void Ast_release_exp(struct AstNode_exp *e);
void Ast_release_explist(struct AstNode_explist *e);
void Ast_release_varlist(struct AstNode_varlist *v);

void Ast_release_condlist(struct AstNode_condlist *condlist);
void Ast_release_namelist(struct AstNode_namelist *namelist);
void Ast_release_funcbody(struct AstNode_funcbody *funcbody);;

void Ast_release_fieldlist(struct AstNode_fieldlist *fieldlist);
void Ast_release_params(struct AstNode_params *params);

void Ast_release_ublock(struct AstNode_ublock *ublock);
void Ast_release_repetition(struct AstNode_repetition *repetition);
void Ast_release_args(struct AstNode_args *args);

void AstToken_release(struct AstToken *t);

#endif                          /* end of file */
