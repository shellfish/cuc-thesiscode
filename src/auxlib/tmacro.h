#ifndef LIBAUX_PREPROCESSOR_INCLUDE
#define LIBAUX_PREPROCESSOR_INCLUDE

#include <assert.h>
#include <stddef.h>
#define ALLOC(a, size) ({__typeof__(a) _a; \
		_a  = (__typeof__(_a)) malloc(sizeof(*(_a)) * size);\
		assert(_a);   _a; })

#define MALLOC(size) ({void *_a; \
		_a  = malloc(size); \
        assert(_a);   _a; })

#define NEW(a)	({ ALLOC(a, 1); })

#define ALLOCATE(a, size, opaque, allocator) ({__typeof__(a) _a; \
    _a  = (__typeof__(_a)) (allocator)(opaque, (sizeof(*(_a)) * size));\
    assert(_a);   _a; })
#define DEALLOCATE(p, opaque, releaser) do {\
    (releaser)((opaque, p)); } while (0)

#define CREATE(a, opaque, allocator) ({ ALLOCATE(a, 1, opaque, allocator); })


#define REALLOC(a, size) ({__typeof__(a) _a = (a); \
		_a = (__typeof__(_a)) realloc((_a), sizeof(*(_a)) * size);\
		assert(_a);  _a; })

#define FREE(a) ({ free((void*)(a)); })

#define ARRAYSIZE(a) (sizeof((a)) / sizeof((a)[0]))
#define offset_of(type, member) ((size_t) &(((type*)0)->member))
/*   #define offset_of(type, member) offsetof((type), (member)) */
#define container_of(ptr, type, member) \
	(type*) ( (char*)(ptr) - offset_of(type, member) )

#define return_error(c) return(c)


#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /* @unused@*/ x
#else
# define UNUSED(x) x
#endif

#endif
