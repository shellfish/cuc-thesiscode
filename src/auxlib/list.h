#ifndef LIBAUX_LIST_INCLUDE
#define LIBAUX_LIST_INCLUDE

/* retieve from linux list.h  */

#define list_entry(ptr, type, member) \
	((type*) container_of(ptr, type, member))

#define LIST_HEAD(name) \
	struct list name = {&(name), &(name)}
struct list;
/* circle double-link list  */
struct list {
    struct list *next;
    struct list *prev;
};

static inline void list_init(struct list *list)
{
    list->next = list;
    list->prev = list;
}

/* insert after head */
static inline void list_insert(struct list *l, struct list *newl)
{
    newl->next = l->next;
    newl->prev = l;
    l->next = newl;
    newl->next->prev = newl;
}

static inline struct list *list_next(struct list *l)
{
    return l->next;
}

static inline struct list *list_prev(struct list *l)
{
    return l->prev;
}

static inline struct list *list_concat(struct list *a, struct list *b)
{
    struct list *tail_a = a->prev;
    struct list *tail_b = b->prev;
    tail_a->next = b;
    a->prev = tail_b;
    b->prev = tail_a;
    tail_b->next = a;
    return a;
}

#define NEXT(a) ({\
			list_entry((a)->link.next, __typeof__(*(a)), link);\
		})

#endif
