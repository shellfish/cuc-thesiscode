#define TINA_LIB
#define tavl_c

#include <string.h>
#include <stdlib.h>
#include <alloca.h>


#include "tavl.h"
#include "tmacro.h"

#define AVL_STACK_SIZE 27



#define exchange(a, b) do {\
		void *tmp = a->data; \
		a->data = b->data; \
		b->data = tmp; \
} while (0)
#define ll_rotate() do { \
	tmp = r->left;\
	r->left = tmp->right;\
	tmp->bf  = r->bf  = 0;\
	tmp->right = r;\
} while(0)
#define rr_rotate() do { \
	tmp = r->right;\
	r->right = tmp->left;\
	tmp->left = r;\
	tmp->bf = r->bf = 0;\
} while (0)
#define rl_rotate() do { \
		tinaU_AvlNode *pivot = r->right;\
		tmp = pivot->left;\
		pivot->left = tmp->right;\
		tmp->right = pivot;\
		r->right = tmp->left;\
		tmp->left = r;\
		switch (tmp->bf) {\
			case 0:\
				pivot->bf = r->bf = 0; \
				break;\
			case 1:\
				r->bf = 0;\
				pivot->bf = -1;\
				break;\
			case -1:\
				r->bf = 1;\
				pivot->bf = 0;\
				break;\
		}\
		tmp->bf = 0;\
} while (0)
#define lr_rotate() do { \
		tinaU_AvlNode *pivot = r->left;\
		tmp = pivot->right;\
		pivot->right = tmp->left;\
		tmp->left = pivot;\
		r->left = tmp->right;\
		tmp->right = r;\
		switch(tmp->bf) {\
			case 0:\
				pivot->bf = r->bf = 0; break;\
			case 1:\
				pivot->bf = 0; \
				r->bf = -1;\
				break;\
			case -1:\
				pivot->bf = 1;\
				r->bf = 0;\
				break;\
		}\
		tmp->bf = 0;\
} while (0)

static tinaU_COMPARER(default_comparer);
static tinaU_ALLOCATOR(default_allocator);
static tinaU_RELEASER(default_releaser);
static tinaU_NODE_INITIALIZER(default_node_initializer);

static void init_node(tinaU_AvlTree *t, tinaU_AvlNode * n)
{
    n->bf = 0;
    n->left = n->right = NULL;
    if (t->node_initializer)
        t->node_initializer(&n->data);
}

void  tinaU_initAvl(tinaU_AvlTree *t)
{
    t->comparer = default_comparer;
    t->node_initializer = default_node_initializer;
    tinaU_avlSetAllocator(t, NULL, default_allocator);
    tinaU_avlSetReleaser(t, NULL, default_releaser);
    t->root = NULL;
    t->count = 0;
}

tinaU_AvlNode *tinaU_avlFind(tinaU_AvlTree * t, const void *data)
{
    tinaU_AvlNode *n = t->root;
    int r;
    while (n) {
        r = t->comparer(data, n->data);
        if (r != 0) {
            n = (r > 0) ? n->right : n->left;
        } else {
            return n;
        }
    }
    return NULL;
}

tinaU_AvlNode *tinaU_avlInsert(tinaU_AvlTree * t, const void *data)
{
    /*  the node to be insrted  */
    tinaU_AvlNode *new_node =  CREATE(new_node, t->allocator_opaque, t->allocator);
    new_node->data = NULL;      /*  mark new de as empty, NULL pointer */
    t->count++;
    init_node(t, new_node);

    tinaU_AvlNode *r = t->root;
    tinaU_AvlNode **p_root = &t->root;
    if (r == NULL) {
        t->root = new_node;
        return new_node;
    }

    tinaU_AvlNode **real_buf =
        (__typeof__(real_buf)) alloca(AVL_STACK_SIZE * sizeof (*real_buf));
    tinaU_AvlNode **p_buf = real_buf;

    while (r) {
        *p_buf++ = r;
        if (t->comparer(data, r->data) < 0)
            r = r->left;
        else
            r = r->right;
    }
    r = *(p_buf - 1);
    if (t->comparer(data, r->data) < 0) {
        r->left = new_node;
    } else {
        r->right = new_node;
    }

    while (--p_buf >= real_buf) {
        r = *p_buf;
        r->bf += (t->comparer(data, r->data) < 0) ? 1 : -1;
        tinaU_AvlNode *tmp;
        switch (r->bf) {
        case 2:                // left case
            if (r->left->bf == 1) { /* left-left case  */
                ll_rotate();
            } else {            /* r->left->bf == -1 || left-right case  */
                lr_rotate();
            }
            break;
        case -2:               // right case
            if (r->right->bf == -1) {   /* right-right case  */
                rr_rotate();
            } else {            /* r->right->bf == 1 || right-left case  */
                rl_rotate();
            }
            break;
        case 0:
            return new_node;
        default:
            continue;
        }
        /* mount rebalanced subtree  */
        tinaU_AvlNode **p_parent;
        if (p_buf == real_buf)
            p_parent = p_root;
        else {
            p_buf--;
            p_parent =
                t->comparer(data,
                          (*p_buf)->data) <
                0 ? &(*p_buf)->left : &(*p_buf)->right;
        }
        *p_parent = tmp;
        return new_node;
    }
    return new_node;
}

tinaU_AvlNode *tinaU_avlReplace(tinaU_AvlTree * t, const void *data)
{

    tinaU_AvlNode *r = t->root;
    tinaU_AvlNode **p_root = &t->root;
    tinaU_AvlNode **real_buf;
    real_buf =
        (__typeof__(real_buf)) alloca(AVL_STACK_SIZE * sizeof (*real_buf));
    tinaU_AvlNode **p_buf = real_buf;

    if (!r) {
        tinaU_AvlNode *new_node =  CREATE(new_node, t->allocator_opaque, t->allocator);
        init_node(t, new_node);
        t->count++;
        new_node->data = NULL;
        t->root = new_node;
        return new_node;
    }

    int c;
    while (r) {
        *p_buf++ = r;
        c = t->comparer(data, r->data);
        if (c < 0) {
            r = r->left;
        } else if (c > 0) {
            r = r->right;
        } else {
            /* just return the original node */
            return r;
        }
    }
    r = *(p_buf - 1);
    /*  the node to be insrted  */
    tinaU_AvlNode *new_node = CREATE(new_node, t->allocator_opaque, t->allocator);
    init_node(t, new_node);
    t->count++;
    if (t->comparer(data, r->data) < 0) {
        r->left = new_node;
    } else {
        r->right = new_node;
    }

    while (--p_buf >= real_buf) {
        r = *p_buf;
        r->bf += (t->comparer(data, r->data) < 0) ? 1 : -1;
        tinaU_AvlNode *tmp;
        switch (r->bf) {
        case 2:                // left case
            if (r->left->bf == 1) { /* left-left case  */
                ll_rotate();
            } else {            /* r->left->bf == -1 || left-right case  */
                lr_rotate();
            }
            break;
        case -2:               // right case
            if (r->right->bf == -1) {   /* right-right case  */
                rr_rotate();
            } else {            /* r->right->bf == 1 || right-left case  */
                rl_rotate();
            }
            break;
        case 0:
            goto end;
        default:
            continue;
        }
        /* mount rebalanced subtree  */
        tinaU_AvlNode **p_parent;
        if (p_buf == real_buf)
            p_parent = p_root;
        else {
            p_buf--;
            p_parent =
                t->comparer(data,
                          (*p_buf)->data) <
                0 ? &(*p_buf)->left : &(*p_buf)->right;
        }
        *p_parent = tmp;
        goto end;
    }

  end:
    new_node->data = NULL;
    return new_node;
}

/////////////////////////////////////

tinaU_AvlNode *tinaU_avlSteal(tinaU_AvlTree * t, const void *data)
{
    tinaU_AvlNode **real_buf =
        (__typeof__(real_buf)) alloca(AVL_STACK_SIZE * sizeof (*real_buf));
    tinaU_AvlNode **p_buf = real_buf - 1;   // reset buffer
    tinaU_AvlNode **p_parent;
    tinaU_AvlNode **split_buf;
    tinaU_AvlNode **p_root = &t->root;
    tinaU_AvlNode *root = t->root;
    tinaU_AvlNode *r;
    tinaU_AvlNode *tmp = NULL;
    tinaU_AvlNode *loot;

    while (root && t->comparer(data, root->data)) {
        *++p_buf = root;
        root = t->comparer(data, root->data) < 0 ? root->left : root->right;
    }
    if (!root)
        return NULL;            // not find

    t->count--;

    p_parent = (p_buf < real_buf) ? p_root :
        (t->comparer(data, (*p_buf)->data) <
         0 ? &(*p_buf)->left : &(*p_buf)->right);
    if (root->left && !root->right) {
        *p_parent = root->left;
        loot = root;
        split_buf = p_buf + 1;
    } else if (root->left && root->right) {
        *++p_buf = root;
        split_buf = p_buf;
        r = root->left;
        while (r->right) {
            *++p_buf = r;
            r = r->right;
        }
        // mount
        if (*p_buf == root)
            (*p_buf)->left = r->left;
        else
            (*p_buf)->right = r->left;

        exchange(root, r);
        loot = r;
    } else {
        /* else if (!root->left && (root->right || !root->right))  */
        *p_parent = root->right;
        loot = root;
        split_buf = p_buf + 1;
    }
    // backtrace
    int goon_backtrace = 1;
    for (; p_buf >= real_buf && goon_backtrace; p_buf--) {
        r = *p_buf;
        if (p_buf < split_buf)
            r->bf += t->comparer(data, r->data) < 0 ? -1 : +1;
        else if (p_buf == split_buf)    // r point to orig delete node
            r->bf += -1;
        else
            r->bf += 1;

        switch (r->bf) {
        case -1:
        case +1:               /* stop backtrace  */
            return loot;
        case 0:
            continue;
        case 2:                // left case
            if (r->left->bf == 1) { /* left-left case  */
                ll_rotate();
            } else if (r->left->bf == -1) {
                lr_rotate();
            } else {            /* r->left->bf == 0  */
                goon_backtrace = 0;
                tmp = r->left;
                r->left = tmp->right;
                tmp->right = r;
                tmp->bf = -1;
                r->bf = 1;
            }
            break;
        case -2:               // right case
            if (r->right->bf == -1) {   /* right-right case  */
                rr_rotate();
            } else if (r->right->bf == 1) {
                rl_rotate();
            } else {            /* r->right->bf == 0  */
                goon_backtrace = 0;
                tmp = r->right;
                r->right = tmp->left;
                tmp->left = r;
                tmp->bf = 1;
                r->bf = -1;
            }
            break;
        }                       // end switch
        /* mount rebalanced subtree  */
        if (p_buf == real_buf)
            p_parent = p_root;
        else {
            tinaU_AvlNode *p = *(p_buf - 1);
            if (p_buf <= split_buf)
                p_parent = t->comparer(data, p->data) < 0 ? &p->left : &p->right;
            else if (p_buf == split_buf + 1)
                p_parent = &p->left;
            else {
                p_parent = &p->right;
            }
        }
        *p_parent = tmp;
    }                           // end for
    return loot;
}

void tinaU_avlRelease(tinaU_AvlTree * t, tinaU_AvlNode * node)
{
    if (t->releaser)
        t->releaser(t->releaser_opaque, node);
    FREE(node);
}

void tinaU_avlDelete(tinaU_AvlTree * t, void *data)
{
    tinaU_AvlNode *n = tinaU_avlSteal(t, data);
    tinaU_avlRelease(t, (tinaU_AvlNode *) n->data);
}

static void apply(tinaU_AvlNode * root, void *opaque, tinaU_Releaser func)
{
    if (root) {
        if (root->left)
            apply(root->left, opaque, func);
        func(opaque, root);
        tinaU_AvlNode *r = root->right;
        if (r)
            apply(r, opaque, func);
    }
}

void tinaU_freeAvl(tinaU_AvlTree * t)
{
    if (t->releaser)
        apply(t->root, t->releaser_opaque, t->releaser);
}

tinaU_COMPARER(default_comparer)
{
    if (dest > src)
        return 1;
    else if (dest < src)
        return -1;
    else
        return 0;
}


tinaU_NODE_INITIALIZER(default_node_initializer)
{
    *pdata  = NULL;
}


tinaU_ALLOCATOR(default_allocator)
{
    return malloc(sz);
}

tinaU_RELEASER(default_releaser)
{
    free(node);
}


void  tinaU_avlSetReleaser(tinaU_AvlTree *t
                           ,void *releaser_opaque
                           , tinaU_Releaser releaser)
{
    t->releaser_opaque = releaser_opaque;
    t->releaser = releaser;
}

void  tinaU_avlSetAllocator(tinaU_AvlTree *t
                          ,void *allocator_opaque
                          , tinaU_Allocator allocator)
{
    t->allocator_opaque = allocator_opaque;
    t->allocator = allocator;
}



