#define TINA_LIB
#define tstring_c

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "tstring.h"
#include "tmacro.h"

#define DEFAULT_BUFSIZE 33

tinaU_String *tinaU_newStr(size_t leng)
{
    assert(leng >= 0);
    size_t space = (leng == 0) ? DEFAULT_BUFSIZE : (leng + 1);
    tinaU_String *s;
    s = NEW(s);
    s->space = space;
    s->buf = ALLOC(s->buf, space);
    s->buf[0] = '\0';
    s->leng = 0;
    return s;
}

tinaU_String *tinaU_addStr(tinaU_String * s, const char *src, size_t leng)
{
    size_t expect_leng = s->leng + leng;
    size_t expect_space = expect_leng + 1;
    if (expect_space <= s->space) {
        memcpy(s->buf + s->leng, src, leng + 1);
    } else {                    /* expand tinaU_String */
        size_t double_space = s->space * 2;
        s->space = (double_space > expect_space) ? double_space : expect_space;
        s->buf = REALLOC(s->buf, s->space);
        memcpy(s->buf + s->leng, src, leng + 1);
    }
    s->leng = expect_leng;
    return s;
}

tinaU_String *tinaU_catStr(tinaU_String * s, const char *src)
{
    size_t leng = strlen(src);
    tinaU_addStr(s, src, leng);
    return s;
}

tinaU_String *tinaU_addByte(tinaU_String * s, char byte)
{
    if (s->space == s->leng + 1) {
        s->space *= 2;
        s->buf = REALLOC(s->buf, s->space);
    }
    s->buf[s->leng++] = byte;
    return s;
}

void tinaU_destroyStr(tinaU_String * s, char **pbuf)
{
    if (pbuf)
        *pbuf = s->buf;
    else
        free(s->buf);
    free(s);
}

tinaU_String *tinaU_quoteStr(const char *str)
{
    tinaU_String *s = tinaU_newStr(0);
    char buf[5] = { '\\' };
    str--;
    while ((*++str) != '\0') {
        if (isgraph(*str)) {
            tinaU_addByte(s, *str);
        } else {
            switch (*str) {
            case '\a':
                buf[1] = 'a';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\t':
                buf[1] = 't';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\n':
                buf[1] = 'n';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\f':
                buf[1] = 'f';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\r':
                buf[1] = 'r';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\"':
                buf[1] = '\"';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '\'':
                buf[1] = '\'';
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            case '<':
            case '>':
            case '}':
            case '{':
            case ' ':
            case '|':
            case '[':
            case ']':
            case '(':
            case ')':
                buf[1] = *str;
                buf[2] = '\0';
                tinaU_addStr(s, buf, 2);
                break;
            default:
                snprintf(buf + 1, 4, "%3o", *str);
                tinaU_addStr(s, buf, 4);
                break;
            }
        }
    }
    return s;
}
