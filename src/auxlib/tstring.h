#ifndef tstring_h
#define tstring_h

#include <stddef.h>

#ifndef TINA_LIB
struct tinaU_String {
    char *buf;
    size_t leng;
};
#else
struct tinaU_String {
    char *buf;
    size_t leng;
    size_t space;
};
#endif

#ifndef TINA_STRUCT_TINAU_STRING
#define TINA_STRUCT_TINAU_STRING
typedef struct tinaU_String tinaU_String;
#endif

/* @leng : the initinal buffer space */
tinaU_String *tinaU_newStr(size_t leng);

/* @leng: the return value of strlen(@src) */
tinaU_String *tinaU_addStr(tinaU_String * s, const char *c, size_t leng);
tinaU_String *tinaU_catStr(tinaU_String * s, const char *c);
tinaU_String *tinaU_addByte(tinaU_String * s, char byte);

tinaU_String *tinaU_quoteStr(const char *s);

/*
 * if you want to cut off the internal buffer
 * feel free to assign value to the leng field of internal structure
 * like `sb->leng = 0;` will truncate stringbuffer to 0 but hs no influence
 * the capacity of this buffer
 *  */
void tinaU_destroyStr(tinaU_String * s, char **pbuf);

#endif
