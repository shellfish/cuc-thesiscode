#ifndef tavl_h
#define tavl_h

#include <stddef.h>

typedef struct tinaU_AvlTree tinaU_AvlTree;
typedef struct tinaU_AvlNode tinaU_AvlNode;

/* compare between node->data  */
#define tinaU_COMPARER(proc) int proc(const void *dest, const void *src)
typedef tinaU_COMPARER((*tinaU_Comparer));

/* be called when before deleting node  */
#define tinaU_RELEASER(proc) void proc(void *releaser_opaque \
                            , tinaU_AvlNode *node)
typedef tinaU_RELEASER((*tinaU_Releaser));

/* be used to allocate space for tree node  */
#define tinaU_ALLOCATOR(proc) void *proc(void *allocator_opaque \
                            , size_t sz)
typedef tinaU_ALLOCATOR((*tinaU_Allocator));

/* wiil be called every time when new node is created  */
#define tinaU_NODE_INITIALIZER(proc) void proc(void **pdata)
typedef tinaU_NODE_INITIALIZER((*tinaU_NodeInitializer));


struct tinaU_AvlNode {
    void *data;
    int bf;
    tinaU_AvlNode *left;
    tinaU_AvlNode *right;
};

struct tinaU_AvlTree {
    size_t                  count;  /* count number of node  */
    tinaU_NodeInitializer   node_initializer;
    void                   *allocator_opaque;
    tinaU_Allocator         allocator;
    void                   *releaser_opaque;
    tinaU_Releaser          releaser;
    tinaU_Comparer          comparer;
    tinaU_AvlNode          *root;
};

void            tinaU_initAvl(tinaU_AvlTree *t);
void            tinaU_freeAvl(tinaU_AvlTree * t);

void            tinaU_avlSetReleaser(tinaU_AvlTree *t
                                    ,void *releaser_opaque
                                    , tinaU_Releaser releaser);
void            tinaU_avlSetAllocator(tinaU_AvlTree *t
                                    ,void *allocator_opaque
                                    , tinaU_Allocator allocator);
tinaU_AvlNode  *tinaU_avlFind(tinaU_AvlTree * t, const void *data);
tinaU_AvlNode  *tinaU_avlSteal(tinaU_AvlTree * t, const void *data);
tinaU_AvlNode  *tinaU_avlInsert(tinaU_AvlTree * t, const void *data);
tinaU_AvlNode  *tinaU_avlReplace(tinaU_AvlTree * t, const void *data);

/*  data has no constant specifier, for the releaser may change its content */
void            tinaU_avlDelete(tinaU_AvlTree * t, void *data);
void            tinaU_avlRelease(tinaU_AvlTree * t, tinaU_AvlNode * node);

#endif
