#ifndef tinadef_h
#define tinadef_h

#include <stddef.h>

#define TINA_PROC_OUPUT(proc) void proc(void *opaque\
        , const void *data, size_t sz)
typedef TINA_PROC_OUPUT((*tina_OuputFunc));

#define TINA_PROC_MSG(proc) void proc(void *opaque, const char *msg)
typedef TINA_PROC_MSG((*tina_MsgFunc));

/* avoid double typedef   */
#ifndef TINA_STRUCT_AST_TOKEN
#define TINA_STRUCT_AST_TOKEN
struct AstToken;
typedef struct AstToken tinaA_Token;
#endif

#ifndef TINA_STRUCT_AST_ROOT
#define TINA_STRUCT_AST_ROOT
struct AstNode_statlist;
typedef struct AstNode_statlist tinaA_Root;
#endif

#ifndef TINA_STRUCT_TINAU_STRING
#define TINA_STRUCT_TINAU_STRING
typedef struct tinaU_String tinaU_String;
#endif

#endif
