/* vim: ft=lemon
    * * check out from  luawiki:
    * * http://lua-users.org/wiki/LuaGrammar
    */
%include {
#include <stdio.h>
#include <stdlib.h>

#include "auxlib/list.h"

#include "tina.h"
#include "tast.h"

/* internal utility functions */
#define gen_error(a) do {\
    pextra->error_func(pextra->error_opaque, (a)); \
} while (0)

#define CONCAT(a, b) ({ \
    void *buf;\
    __typeof__(a) _a = (a);\
    __typeof__(b) _b = (b);\
    if (! _a) buf = (void*)_b;\
    else if (!_b) buf = (void*)_a;\
    else {\
        buf = (void*)_a;\
        list_concat(&_a->link, &_b->link);\
    }\
    (__typeof__(_a))buf;\
})



}

%token_prefix TK_
%extra_argument {ParserState *pextra}

%syntax_error {
    gen_error("Syntax error");
}

%parse_failure {
    pextra->status= -1;
}

%parse_accept {
    pextra->status = 1;
}

%token_type {struct AstToken *}

%type chunk {struct AstNode_statlist *}
%destructor chunk {Ast_release_statlist($$); }

%type block {struct AstNode_statlist *}
%destructor block {Ast_release_statlist($$); }

%type scope {struct AstNode_statlist *}
%destructor scope {Ast_release_statlist($$); }

%type statlist {struct AstNode_statlist *}
%destructor statlist {Ast_release_statlist($$); }

%type laststat {struct AstNode_statlist*}
%destructor laststat {Ast_release_statlist($$); }

%type binding {struct AstNode_statlist *}
%destructor binding {Ast_release_statlist($$); }

%type stat {struct AstNode_statlist *}
%destructor stat {Ast_release_statlist($$); }

%type exp {struct AstNode_exp *}
%destructor exp {Ast_release_exp($$); }

%type conds {struct AstNode_condlist *}
%destructor conds {Ast_release_condlist($$); }

%type funcbody {struct AstNode_funcbody *}
%destructor funcbody {Ast_release_funcbody($$); }

%type funcname {struct AstNode_namelist *}
%destructor funcname {Ast_release_namelist($$); }

%type setlist {struct AstNode_setlist *}
%destructor setlist {Ast_release_setlist($$); }

%type explist1 {struct AstNode_explist *}
%destructor explist1 {Ast_release_explist($$); }

%type explist23 {struct AstNode_explist *}
%destructor explist23 {Ast_release_explist($$); }

%type namelist {struct AstNode_namelist *}
%destructor namelist {Ast_release_namelist($$); }

%type prefixexp {struct AstNode_varlist *}
%destructor prefixexp {Ast_release_varlist($$); }

%type tableconstructor {struct AstNode_fieldlist *}
%destructor tableconstructor {Ast_release_fieldlist($$); }

%type functioncall {struct AstNode_varlist *}
%destructor functioncall {Ast_release_varlist($$); }

%type params {struct AstNode_params *}
%destructor params {Ast_release_params($$); }

/*intermedia object */
%type ublock {struct AstNode_ublock *}
%destructor ublock {Ast_release_ublock($$); }

%type repetition {struct AstNode_repetition *}
%destructor repetition {Ast_release_repetition($$); }

%type condlist {struct AstNode_condlist *}
%destructor condlist { Ast_release_condlist($$); }

%type cond {struct AstNode_condlist *}
%destructor cond { Ast_release_condlist($$); }

%type dottedname {struct AstNode_namelist *}
%destructor dottedname {Ast_release_namelist($$); }

%type fieldlist {struct AstNode_fieldlist *}
%destructor fieldlist { Ast_release_fieldlist($$); }

%type var {struct AstNode_varlist *}
%destructor var {Ast_release_varlist($$) ; }

%type args { struct AstNode_args *}
%destructor args { Ast_release_args($$); }

%type parlist {struct AstNode_params *}
%destructor parlist { Ast_release_params($$);}

%type field {struct AstNode_fieldlist *}
%destructor field { Ast_release_fieldlist($$); }

%type function {struct AstNode_funcbody *}
%destructor function { Ast_release_funcbody($$); }

%token_destructor {
    AstToken_release($$);
}


%left      OR .
%left      AND .
%left      LT LE GT GE EQ NE .
%right     CONCAT .
%left      PLUS MINUS .
%left      TIMES DIVIDE MOD .
%right     NOT POUND.
%right     EXP.

%fallback  OPEN LEFT_PARENTHESIS .

%start_symbol chunk
chunk   ::= block(B). {
        pextra->root = B;
}

semi       ::= SEMICOLON .
semi       ::= .

block(A)   ::= scope(B) statlist(C) . {
    A = CONCAT(B, C);
}

block(A)  ::= scope(B) statlist(C) laststat(D) semi . {
    A = CONCAT(CONCAT(B, C), D);
}

ublock(A)  ::= block(B) UNTIL exp(C) . {
    A = NEW(A);
    A->statlist = B;
    A->exp = C;
}

scope(A)   ::= . {
    A = NULL;
}

scope(A)  ::= scope(B) statlist(C) binding(D) semi. {
    A = CONCAT(CONCAT(B, C), D);
}

statlist(A)  ::= . {
    A = NULL;
}

statlist(A)  ::= statlist(B) stat(C) semi . {
    A = CONCAT(B, C);
}

stat(A)   ::= DO block(B) END . {
    CREATE_STAT(doblock, sl, A, STAT_DOBLOCK);
    sl->statlist = B;
}

stat(A)  ::= WHILE exp(B) DO block(C) END . {
    CREATE_STAT(while, wl, A, STAT_WHILE);
    wl->exp = B;
    wl->statlist= C;
}

stat	  ::= WHILE exp error block END. {
       gen_error("'do' expected near 'while'"); 
}

stat(A)  ::= repetition(B) /* discard  */ DO block(C) END . {
    assert(B->explist);
    if (B->name && !B->namelist) {  // enum for
        CREATE_STAT(enum_for, s, A, STAT_ENUM_FOR);

    s->name = B->name;
            s->explist = B->explist;
            s->statlist= C;
            FREE(B);
        } else if (!B->name && B->namelist) { // generic for
            CREATE_STAT(generic_for, s, A, STAT_GENERIC_FOR);

    s->namelist = B->namelist;
            s->explist = B->explist;
            s->statlist= C;
            FREE(B);
        } else
            assert(0);
}

stat(A)   ::= REPEAT ublock(B) /* discard  */ . {
    CREATE_STAT(repeat, s, A, STAT_REPEAT);

    s->statlist = B->statlist;
    s->exp = B->exp;
    FREE(B);
}

stat(A)   ::= IF conds(B) END . {
    CREATE_STAT(cond, s, A, STAT_COND);
    s->condlist = B;
}

stat	   ::= IF error END .  { 
        gen_error("'then' expected near 'if'");
}

stat(A)  ::= FUNCTION funcname(B) funcbody(C) . {
    CREATE_STAT(func, s, A, STAT_FUNC);

    s->funcname = B;
    s->funcbody = C;
}

stat(A) ::= setlist(B) ASSIGN explist1(C) . {
    CREATE_STAT(assign, s, A, STAT_ASSIGN);
    s->setlist = B;
    s->explist = C;
}

stat(A)   ::= functioncall(B) . {
    CREATE_STAT(funcall, s, A, STAT_FUNCALL);
    s->funcall = B;
}

repetition(A) ::= FOR NAME(B) ASSIGN explist23(C) . {
    struct AstNode_repetition *r = NEW(r);
    r->explist = C;
    r->name = B->body.name; FREE(B);
    r->namelist = NULL;
    A = (struct AstNode_repetition*)r;
}

repetition(A) ::= FOR namelist(B) IN explist1(C) . {
    struct AstNode_repetition *r = NEW(r);
    r->explist = C;
    r->name = NULL;
    r->namelist = B;
    A = (struct AstNode_repetition*)r;
}

conds(A)   ::= condlist(B) . {
    A = B;
}

conds(A)   ::= condlist(B) ELSE block(C) . {
    struct AstNode_condlist *co = NEW(co);
    list_init(&co->link);
    co->statlist = C;
    co->exp = NULL;  // else clause
    A = CONCAT(B, co);
}

condlist(A)   ::= cond(B) . {
    A = B;
}

condlist(A)   ::= condlist(B) ELSEIF cond(C) . {
    A = CONCAT(B, C);
}

cond(A)   ::= exp(B) THEN block(C) . {
    A = NEW(A);
    list_init(&A->link);
    A->statlist = C;
    A->exp = B;
}

laststat(A)   ::= BREAK . {
    A = NEW(A);
    list_init(&A->link);
    A->type = STAT_BREAK;
}

laststat(A)   ::= RETURN. {
    CREATE_STAT(leave, s, A, STAT_RETURN);
    s->explist = NULL;
}

laststat(A)   ::= RETURN explist1(C) . {
    CREATE_STAT(leave, s, A, STAT_RETURN);
    s->explist = C;
}

binding(A)  ::= LOCAL namelist(B) . {
    CREATE_STAT(decl, s, A, STAT_DECL);
    s->namelist = B;
    s->explist = NULL;
}

binding(A) ::= LOCAL namelist(B) ASSIGN explist1(C) . {
    CREATE_STAT(decl, s, A, STAT_DECL);
    s->namelist = B;
    s->explist = C;
}

binding(A)  ::= LOCAL FUNCTION NAME(B) funcbody(C) . {
    CREATE_STAT(decl_function, s, A, STAT_DECL_FUNC);
    s->name = B->body.name; FREE(B);
    s->funcbody = C;
}

funcname(A)   ::= dottedname(B) . {
    A = B;
}

funcname(A)   ::= dottedname(B) COLON NAME(C) . {
    struct AstNode_namelist *n = NEW(n), *m = NEW(m);
    list_init(&n->link); list_init(&m->link);
    n->name = NULL; m->name = C->body.name; FREE(C);
    A = CONCAT(CONCAT(B, n), m);
}


dottedname(A) ::= NAME(B) . {
    A = NEW(A);
    list_init(&A->link);
    A->name = B->body.name; FREE(B);
}

dottedname(A) ::= dottedname(B) PERIOD NAME(C) . {
    struct AstNode_namelist *n = NEW(n);
    list_init(&n->link);
    n->name = C->body.name; FREE(C);
    A = CONCAT(B, n);
}

namelist(A)   ::= NAME(B) . {
    A = NEW(A);
    list_init(&A->link);
    A->name = B->body.name; FREE(B);
}

namelist(A)   ::= namelist(B) COMMA NAME(C) . {
    struct AstNode_namelist *n = NEW(n);
    list_init(&n->link);
    n->name = C->body.name; FREE(C);
    A = CONCAT(B, n);
}

explist1(A)   ::= exp(B) . {
    A = NEW(A);
    list_init(&A->link);
    A->exp = B;
}


explist1(A)   ::= explist1(B) COMMA exp(C) . {
    struct AstNode_explist *e = NEW(e);
    list_init(&e->link);
    e->exp = C;
    A = CONCAT(B, e);
}

explist23(A)  ::= exp(B) COMMA exp(C) . {
    struct AstNode_explist *e1 = NEW(e1), *e2 = NEW(e2);
    list_init(&e1->link); list_init(&e2->link);
    e1->exp = B; e2->exp =C;
    A = CONCAT(e1, e2);
}

explist23(A)  ::= exp(B) COMMA exp(C) COMMA exp(D) . {
    struct AstNode_explist *e1 = NEW(e1), *e2 = NEW(e2), *e3 = NEW(e3);
    list_init(&e1->link); list_init(&e2->link); list_init(&e3->link);
    e1->exp = B; e2->exp =C; e3->exp  = D;
    A = CONCAT(e1, CONCAT(e2, e3));
}

exp(A) ::= NIL . {
    A = NEW(A); A->type = EXP_NIL;
}


exp(A) ::= TRUE. {
    A = NEW(A); A->type = EXP_TRUE;
}


exp(A) ::= FALSE. {
    A = NEW(A); A->type = EXP_FALSE;
}

exp(A) ::= VAR_ARG .{
    A = NEW(A); A->type = EXP_VARARG;
}


exp(A) ::= NUMBER(B) . {
    CREATE_EXP(number, e, A, EXP_NUMBER);
    e->number = B->body.number;
    FREE(B);
}

exp(A) ::= STRING(B) . {
    CREATE_EXP(string, e, A, EXP_STRING);
    e->string= B->body.string;
    FREE(B);
}

exp(A)  ::= function(B) . {
    CREATE_EXP(function, e, A, EXP_FUNCTION);
    e->function  = B;
}

exp(A)  ::= prefixexp(B) . {
    CREATE_EXP(prefix, e, A, EXP_PREFIX);
    e->prefixexp = B;
}

exp (A) ::= tableconstructor(B) . {
    CREATE_EXP(tableconstructor, e, A, EXP_TABLECONSTRUCTOR);
    e->fieldlist = B;
}

exp(A) ::= NOT exp(B) .{
    CREATE_EXP(unary, e, A, EXP_UNARY_NOT);
    e->exp = B;
}

exp(A) ::= MINUS exp(B) .{
    CREATE_EXP(unary, e, A, EXP_UNARY_MINUS);
    e->exp = B;
}


exp(A) ::= POUND exp(B) .{
    CREATE_EXP(unary, e, A, EXP_UNARY_POUND);
    e->exp = B;
}

exp(A)  ::= exp(B) OR exp(D) . {
    CREATE_EXP(binary, e, A, EXP_OR);
    e->exp[0] = B;
    e->exp[1] = D;
}

exp(A)  ::= exp(B) AND exp(D) . {
    CREATE_EXP(binary, e, A, EXP_AND);
    e->exp[0] = B;
    e->exp[1] = D;
}

exp(A) ::= exp(B) LT exp(C) . {
    CREATE_EXP(binary, e, A, EXP_LT);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) LE exp(C) . {
    CREATE_EXP(binary, e, A, EXP_LE);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) GT exp(C) . {
    CREATE_EXP(binary, e, A, EXP_GT);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) GE exp(C) . {
    CREATE_EXP(binary, e, A, EXP_GE);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) EQ exp(C) . {
    CREATE_EXP(binary, e, A, EXP_EQ);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) NE exp(C) . {
    CREATE_EXP(binary, e, A, EXP_NE);
    e->exp[0] = B;
    e->exp[1] = C;
}

exp(A) ::= exp(B) CONCAT exp(C) . {
    CREATE_EXP(binary, e, A, EXP_CONCAT);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) PLUS exp(C) . {
    CREATE_EXP(binary, e, A, EXP_PLUS);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) MINUS exp(C) . {
    CREATE_EXP(binary, e, A, EXP_MINUS);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) TIMES exp(C) . {
    CREATE_EXP(binary, e, A, EXP_TIMES);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) DIVIDE exp(C) . {
    CREATE_EXP(binary, e, A, EXP_DIVIDE);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) MOD exp(C) . {
    CREATE_EXP(binary, e, A, EXP_MOD);
    e->exp[0] = B;
    e->exp[1] = C;
}
exp(A) ::= exp(B) EXP exp(C) . {
    CREATE_EXP(binary, e, A, EXP_EXPONENT);
    e->exp[0] = B;
    e->exp[1] = C;
}

setlist(A)   ::= var(B) . {
    A = NEW(A);
    list_init(&A->link);
    A->varlist = B;
}

setlist(A)    ::= setlist(B) COMMA var(C) . {
    struct AstNode_setlist *s = NEW(s);
    list_init(&s->link);
    s->varlist = C;
    A = CONCAT(B, s);
}

var(A)    ::= NAME(B) . {
    CREATE_VAR(name, v, A, VAR_NAME);
    v->name = B->body.name;
    FREE(B);
}

var(A)    ::= prefixexp(B) LEFT_BRACKET exp(C) RIGHT_BRACKET . {
    CREATE_VAR(subscript, v, A, VAR_SUBSCRIPT);
    v->exp = C;
    A = CONCAT(B, v);
}

var(A)    ::= prefixexp(B) PERIOD NAME(C) . {
    CREATE_VAR(name, v, A, VAR_NAME);
    v->name = C->body.name;
    FREE(C);
    A = CONCAT(B, v);
}

prefixexp(A)  ::= var(B) . {
    A = B;
}
prefixexp(A)  ::= functioncall(B) . {
    A = B;
}

prefixexp(A)  ::= OPEN exp(B) RIGHT_PARENTHESIS . {
    CREATE_VAR(prefixexp, p, A, VAR_PREFIXEXP);
    p->exp = B;
}

functioncall(A) ::= prefixexp(B) args(C)/*discrd*/  . {

if (C->type == ARGS_EMPTY) {
        CREATE_VAR(funcall_exp, v, A, VAR_FUNCALL_EMPTY);
        v->colon_name = NULL;
        v->explist = NULL;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_EXPLIST) {
        CREATE_VAR(funcall_exp, v, A, VAR_FUNCALL_EXP);
        v->colon_name = NULL;
        v->explist = ((struct AstNode_args_explist*)C)->explist;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_TABLECONSTRUCTOR) {
        CREATE_VAR(funcall_table, v, A, VAR_FUNCALL_TABLE);
        v->colon_name = NULL;
        v->fieldlist = ((struct AstNode_args_tableconstructor*)C)->fieldlist;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_STRING) {
        CREATE_VAR(funcall_string, v, A, VAR_FUNCALL_STRING);
        v->colon_name = NULL;
        v->string = ((struct AstNode_args_string*)C)->string;
        A = CONCAT(B, v);
    } else {
        assert(0);
        abort();
    }
    FREE(C);
}

functioncall(A) ::= prefixexp(B) COLON NAME(D) args(C) . {
    if (C->type == ARGS_EMPTY) {
        CREATE_VAR(funcall_exp, v, A, VAR_FUNCALL_EMPTY);
        v->colon_name = D->body.name;
        v->explist = NULL;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_EXPLIST) {
        CREATE_VAR(funcall_exp, v, A, VAR_FUNCALL_EXP);
        v->colon_name = D->body.name;
        v->explist = ((struct AstNode_args_explist*)C)->explist;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_TABLECONSTRUCTOR) {
        CREATE_VAR(funcall_table, v, A, VAR_FUNCALL_TABLE);
        v->colon_name = D->body.name;
        v->fieldlist = ((struct AstNode_args_tableconstructor*)C)->fieldlist;
        A = CONCAT(B, v);
    } else if (C->type == ARGS_STRING) {
        CREATE_VAR(funcall_string, v, A, VAR_FUNCALL_STRING);
        v->colon_name = D->body.name;
        v->string = ((struct AstNode_args_string*)C)->string;
        A = CONCAT(B, v);
    } else {
        assert(0);
        abort();
    }
    FREE(C);
    FREE(D);
}

args(A)        ::= LEFT_PARENTHESIS RIGHT_PARENTHESIS . {
    CREATE_ARGS(explist, a, A, ARGS_EMPTY);
}

args(A)        ::= LEFT_PARENTHESIS explist1(B) RIGHT_PARENTHESIS . {
    CREATE_ARGS(explist, a, A, ARGS_EXPLIST);
    a->explist = B;
}

args(A)        ::= tableconstructor(B) . {
    CREATE_ARGS(tableconstructor, a, A, ARGS_TABLECONSTRUCTOR);
    a->fieldlist = B;
}

args(A)        ::= STRING(B) . {
    CREATE_ARGS(string, a, A, ARGS_STRING);
    a->string = B->body.string;
    FREE(B);
}

function(A)    ::= FUNCTION funcbody(B) . {
    A = B;
}

funcbody(A)    ::= params(B) block(C) END . {
    A = NEW(A);
    A->params = B;
    A->statlist = C;
}

params(A)      ::= LEFT_PARENTHESIS parlist(B) RIGHT_PARENTHESIS . {
    A = B;
}

parlist(A)     ::= . {
    A = NEW(A);
    A->namelist =  NULL;
    A->vararg =  0;
}

parlist(A)     ::= namelist(B) . {
    A = NEW(A);
    A->namelist = B;
    A->vararg = 0;
}

parlist(A)     ::= VAR_ARG . {
    A = NEW(A);
    A->namelist = NULL;
    A->vararg = 1;
}

parlist(A)     ::= namelist(B) COMMA VAR_ARG . {
    A = NEW(A);
    A->namelist = B;
    A->vararg = 1;
}

tableconstructor(A) ::= LEFT_BRACE RIGHT_BRACE . {
    A = NULL;
}

tableconstructor(A) ::= LEFT_BRACE fieldlist(B) RIGHT_BRACE . {
    A = B;
}

tableconstructor(A) ::= LEFT_BRACE fieldlist(B) COMMA|SEMICOLON RIGHT_BRACE . {
    A = B;
}

fieldlist(A)   ::= field(B) . {
    A = B;
}

fieldlist(A)   ::= fieldlist(B) COMMA|SEMICOLON field(C) . {
    A = CONCAT(B, C);
}

field(A)       ::= exp(B) . {
    CREATE_FIELD(only_value, f, A, FIELD_ONLY_VALUE);
    f->exp = B;
}

field(A)       ::= NAME(B) ASSIGN exp(C) . {
    CREATE_FIELD(name_key, f, A, FIELD_NAME_KEY);
    f->exp = C;
    f->name = B->body.name;
    FREE(B);
}

field(A)       ::= LEFT_BRACKET exp(B) RIGHT_BRACKET ASSIGN exp(C) . {
    CREATE_FIELD(index_key, f, A, FIELD_INDEX_KEY);
    f->exp = C;
    f->index = B;
}

%include {

/* declaration of Lemon generated functions */
void *ParseAlloc(void *(*mallocProc)(size_t));
void ParseFree(void *p,   void (*freeProc)(void*));
void Parse(
  void *yyp,                 /* The parser */
  int yymajor,               /* The major token code number */
  tinaA_Token *yyminor,      /* The value for the token */
  ParserState *pextra        /* Optional %extra_argument parameter */
);

static TINA_PROC_MSG(default_error_function)
{
    fprintf((FILE*)opaque, "%s\n", msg);
}

void tinaP_init(ParserState *s)
{
    s->parser =  ParseAlloc(malloc);
    s->lexer_state = NULL;
    s->status = 0;
    s->root = NULL;
    tinaP_set_error_func(s, stderr, default_error_function);
}

void tinaP_set_lexer(ParserState *ps, LexerState *ls)
{
    ps->lexer_state = ls;
}

void tinaP_set_error_func(ParserState *s
    , void *error_opaque,  tina_MsgFunc error_func)
{
    s->error_opaque = error_opaque;
    s->error_func = error_func;
}

tinaA_Root *tinaP_compile(ParserState *s)
{
    tinaA_Token *token;
    int r = tinaL_scan(s->lexer_state, &token);
    for (; r && s->status == 0; r = tinaL_scan(s->lexer_state, &token)) {
        Parse(s->parser, r, token, s);
    }

    Parse(s->parser, 0, NULL, s);

    /* BEGIN:comment out  */
    if (0) {
        if (s->status == 1 && s->root != NULL) {  /* accept  */
            return s->root;
        } else if (s->status == -1 && s->root == NULL) { /* failure  */
            s->error_func(s->error_opaque
                ,tinaE_getstr(MSGP_UNHPOELESS_FAILURE));
            return_error(NULL);
        } else {
            assert(0);
            abort();
        }
    }
    /* END:comment out  */

    if (s->root != NULL) {
        return s->root;
    } else {
        s->error_func(s->error_opaque
            ,tinaE_getstr(MSGP_UNHPOELESS_FAILURE));
        return_error(NULL);
    }
}

void tinaP_free(ParserState *s)
{
    ParseFree(s->parser, free);
    if (s->root) {
        Ast_release_statlist(s->root);
    }
}


}
