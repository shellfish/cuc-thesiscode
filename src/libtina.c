#include "libtina.h"
#include "tina.h"
#include "ttabs.h"

#include "auxlib/tstring.h"
#include <stdlib.h>
#include <string.h>


struct TinaState {
    ParserState parser_state;
    LexerState  lexer_state;
    tina_ConstPool  constant_pool;
    tinaU_String    *string_buffer;
    tina_OuputFunc  output_func;
    void            *output_opaque;
    tinaA_Root      *root;
};

/* create a new Tina compilation context */
TinaState *tina_new(void)
{
    TinaState *tstate = NEW(tstate);
    tinaL_init(&tstate->lexer_state);
    tinaP_init(&tstate->parser_state);

    tinaC_init(&tstate->constant_pool);
    tstate->string_buffer = tinaU_newStr(0);

    tinaL_set_constant_pool(&tstate->lexer_state, &tstate->constant_pool);
    tinaL_set_string_buffer(&tstate->lexer_state, tstate->string_buffer);

    tinaP_set_lexer(&tstate->parser_state, &tstate->lexer_state);

    tstate->output_opaque = NULL;
    tstate->output_func   = NULL;
    return tstate;
}

/* free a Tina compilation context */
void  tina_delete(TinaState *s)
{
    tinaU_destroyStr(s->string_buffer, NULL);
    tinaC_free(&s->constant_pool);
    tinaP_free(&s->parser_state);
    tinaL_free(&s->lexer_state);
    FREE(s);
}

/* set error/warning display callback */
void  tina_set_error_func(TinaState *s, void *error_opaque,
        void (*error_func)(void *opaque, const char *msg))
{
    tinaP_set_error_func(&s->parser_state, error_opaque, error_func);
    tinaL_set_error_func(&s->lexer_state, error_opaque, error_func);
}

/* set output callback */
void  tina_set_output_func(TinaState *s
        , void *output_opaque, tina_OuputFunc output_func)
{
    s->output_func = output_func;
    s->output_opaque = output_opaque;
}

void  tina_set_file_source(TinaState *s, void *file_stream)
{
    tinaL_set_file_source(&s->lexer_state, file_stream);
}

void  tina_set_string_source(TinaState *s, const char *strsrc)
{
    tinaL_set_string_source(&s->lexer_state, strsrc);
}

int   tina_compile(TinaState *s)
{
    s->root = tinaP_compile(&s->parser_state);
    return s->root ? 1 : 0;
}

void  tina_output_bytecode(TinaState *s)
{
    assert(s->output_opaque && s->output_func && s->root);
    tinaG_generate_bytecode(s->root, s->output_opaque, s->output_func);
}

void  tina_output_graphviz(TinaState *s)
{
    assert(s->output_opaque && s->output_func && s->root);
    tinaA_generate_graph(s->root, s->output_opaque, s->output_func);
}

int tina_get_line_number(TinaState *s)
{
    return s->lexer_state.line_number;
}
