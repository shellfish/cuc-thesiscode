#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "tina.h"

/*  keep the enumerate order consistency with `enum tinaE_ConstCode' */
static const char *pool[] = {
    "invalid long string left side",
    "invalid byte sequence",
    "string literal is not proper terminated",
    "lexer meets unknown token",
    "parser failed unhopelessly, exit now"
};

const char *tinaE_getstr(tinaE_ConstCode code)
{
    assert(code >= 0 && code < CONSTCODE_END);
    return pool[code];
}

