#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <alloca.h>
#include <stdarg.h>
#include <string.h>

#include "auxlib/tstring.h"
#include "tina.h"
#include "tast.h"

#define BUFSIZE 12
#define OUTPUT_BUFSIZE 400
#define OUTPUT_CLOSURE void *opaque, tina_OuputFunc  writer
#define OUTPUT_WRITER opaque, writer

#define emit(format, args...) do {\
    char _buf[OUTPUT_BUFSIZE];\
    snprintf(_buf, OUTPUT_BUFSIZE, format, ## args);\
    writer(opaque, _buf, strlen(_buf));\
} while (0)

#define draw_edge(from, flag, to) \
    draw_edge_real(OUTPUT_WRITER, (from), (flag), (to))

static void draw_edge_real(OUTPUT_CLOSURE, void *from
            , const char *flag, void *to)
{
    if (flag)
        emit("_%p:%s -> _%p;\n", from, flag, to);
    else
        emit("_%p -> _%p;\n", from, to);
}



static const char *LITERAL_NULL = "_%p [ "
    "shape=\"triangle\" label=\"NULL\" fontcolor=yellow];\n";
static const char *LITERAL_STR = "_%p [ " "shape=box label=\"%s\" ];\n";
static const char *LITERAL_OPERATOR = "_%p [ "
    "label=\"%s\" fontcolor=blueviolet];\n";
static const char *LITERAL_LITERAL = "_%p [ "
    "shape=\"box\" label=\"\\\"%s\\\"\" fontcolor=purple];\n";

static const char *LITERAL_SIMPLE_STATEMENT = "_%p [ "
    "shape=triangle label=\"%s\" " "color=\".7.3 1.0\" style=filled];\n";


static void visualize_statlist(OUTPUT_CLOSURE, struct AstNode_statlist *st
                             , void *from, const char *flag);
static void visualize_stat(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_decl(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_decl_func(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_doblock(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_while(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_enum_for(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_generic_for(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_repeat(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_cond(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_func(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_leave(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_assign(OUTPUT_CLOSURE, struct AstNode_statlist *s);
static void visualize_stat_funcall(OUTPUT_CLOSURE, struct AstNode_statlist *s);

static void visualize_setlist(OUTPUT_CLOSURE, struct AstNode_setlist *s
                            , void *from, const char *flag);
static void visualize_explist(OUTPUT_CLOSURE, struct AstNode_explist *e
                            , void *from, const char *flag);
static void visualize_exp(OUTPUT_CLOSURE, struct AstNode_exp *exp);
static void visualize_namelist(OUTPUT_CLOSURE, struct AstNode_namelist *n
                            , void *from, const char *flag);
static void visualize_dottedname(OUTPUT_CLOSURE, struct AstNode_namelist *n
                            , void *from, const char *flag);
static void visualize_varlist(OUTPUT_CLOSURE, struct AstNode_varlist *v
                            , void *from, const char *flag);
static void visualize_condlist(OUTPUT_CLOSURE, struct AstNode_condlist *c
                            , void *from, const char *flag);
static void visualize_fieldlist(OUTPUT_CLOSURE, struct AstNode_fieldlist *f
                            , void *from, const char *flag);
static void visualize_funcbody(OUTPUT_CLOSURE, struct AstNode_funcbody *funcbody);
void visualize_params(OUTPUT_CLOSURE, struct AstNode_params *p);

static void visualize(struct AstNode_statlist *block, OUTPUT_CLOSURE)
{
    void *from = (void *) &visualize;   /* use the address of this function as root */
    const char *flag = "root";
    const char *filename = "root of ADT";

    emit("strict digraph G {\n");
    emit("_%p [shape=Mrecord label=\"<%s> %s\"];\n", from, flag, filename);
    visualize_statlist(OUTPUT_WRITER, block, from, flag);
    emit("}\n");
}

void visualize_statlist(OUTPUT_CLOSURE, struct AstNode_statlist *st
                    , void *from, const char *flag)
{
    assert(st);                 /* don't handle NULL in current level  */

    struct AstNode_statlist *p, *next;
    p = st;
    while (1) {
        next = NEXT(p);
        draw_edge(from, flag, p);
        visualize_stat(OUTPUT_WRITER, p);
        if (next == st)
            break;
        p = next;
    }
}

void visualize_stat(OUTPUT_CLOSURE, struct AstNode_statlist *s)
{
    /* first, declare current node: 's'  */
    switch (s->type) {
    case STAT_DECL:
        visualize_stat_decl(OUTPUT_WRITER, s);
        break;
    case STAT_DECL_FUNC:
        visualize_stat_decl_func(OUTPUT_WRITER, s);
        break;
    case STAT_DOBLOCK:
        visualize_stat_doblock(OUTPUT_WRITER, s);
        break;
    case STAT_WHILE:
        visualize_stat_while(OUTPUT_WRITER, s);
        break;
    case STAT_ENUM_FOR:
        visualize_stat_enum_for(OUTPUT_WRITER, s);
        break;
    case STAT_GENERIC_FOR:
        visualize_stat_generic_for(OUTPUT_WRITER, s);
        break;
    case STAT_REPEAT:
        visualize_stat_repeat(OUTPUT_WRITER, s);
        break;
    case STAT_COND:
        visualize_stat_cond(OUTPUT_WRITER, s);
        break;
    case STAT_FUNC:
        visualize_stat_func(OUTPUT_WRITER, s);
        break;
    case STAT_RETURN:
        visualize_stat_leave(OUTPUT_WRITER, s);
        break;
    case STAT_BREAK:
        emit(LITERAL_SIMPLE_STATEMENT, s, "break");
        break;
    case STAT_ASSIGN:
        visualize_stat_assign(OUTPUT_WRITER,s);
        break;
    case STAT_FUNCALL:
        visualize_stat_funcall(OUTPUT_WRITER, s);
        break;
    }
}

void visualize_stat_decl(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_decl *s = (__typeof__(s)) st;

    const char *nf = "namelist";
    const char *ef = "explist";

    emit("_%p [shape=record " "label=\""
         "{ bind local var| { <%s> namelist | <%s> explist }}"
         "\"];\n", s, nf, ef);

    visualize_namelist(OUTPUT_WRITER, s->namelist, s, nf);
    if (!s->explist) {
        draw_edge(s, ef, &s->explist);
        emit(LITERAL_NULL, &s->explist);
    } else
        visualize_explist(OUTPUT_WRITER, s->explist, s, ef);
}

void visualize_stat_decl_func(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_decl_function *s = (__typeof__(s)) st;
    const char *ff = "funcbody";
    emit("_%p [shape=record " "label=\""
         "{ bind local func | {  %s  | <%s> funcbody }}"
         "\"];\n", s, s->name, ff);

    draw_edge(s, ff, s->funcbody);
    visualize_funcbody(OUTPUT_WRITER, s->funcbody);
}

void visualize_stat_doblock(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_doblock *s = (__typeof__(s)) st;
    const char *bf = "block";
    emit("_%p [shape=record " "label=\""
         "do | <%s> block | end" "\"];\n", s, bf);
    if (s->statlist)
        visualize_statlist(OUTPUT_WRITER, s->statlist, s, bf);
    else {
        draw_edge(s, bf, &s->statlist);
        emit(LITERAL_NULL, &s->statlist);
    }
}

void visualize_stat_while(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_while *s = (__typeof__(s)) st;
    const char *ef = "exp";
    const char *bf = "block";

    emit("_%p [shape=record label=\"{"
         "while | { <%s> condition | <%s> block }" "}\"];\n", s, ef, bf);

    draw_edge(s, ef, s->exp);
    visualize_exp(OUTPUT_WRITER, s->exp);

    if (s->statlist) {
        visualize_statlist(OUTPUT_WRITER, s->statlist, s, bf);
    } else {
        emit(LITERAL_NULL, &s->statlist);
        draw_edge(s, bf, &s->statlist);
    }
}

void visualize_stat_enum_for(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_enum_for *s = (__typeof__(s)) st;
    const char *ef = "explist";
    const char *bf = "block";

    emit("_%p [shape=record label=\"{"
         "{ for | %s | = | <%s> explist | do } "
         "| <%s> block " "}\"];\n", s, s->name, ef, bf);

    visualize_explist(OUTPUT_WRITER, s->explist, s, ef);
    if (s->statlist) {
        visualize_statlist(OUTPUT_WRITER, s->statlist, s, bf);
    } else {
        emit(LITERAL_NULL, &s->statlist);
        draw_edge(s, bf, &s->statlist);
    }
}

void visualize_stat_generic_for(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_generic_for *s = (__typeof__(s)) st;
    const char *nf = "namelist";
    const char *ef = "explist";
    const char *bf = "block";
    emit("_%p [shape=record label=\"{"
         "{ for | <%s> namelist | in | <%s> explist}"
         "| <%s> block" "}\"];\n", s, nf, ef, bf);

    visualize_namelist(OUTPUT_WRITER, s->namelist, s, nf);
    visualize_explist(OUTPUT_WRITER, s->explist, s, ef);
    if (s->statlist) {
        visualize_statlist(OUTPUT_WRITER, s->statlist, s, bf);
    } else {
        emit(LITERAL_NULL, &s->statlist);
        draw_edge(s, bf, &s->statlist);
    }
}

void visualize_stat_repeat(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_repeat *s = (__typeof__(s)) st;
    const char *bf = "block";
    const char *ef = "exp";
    emit("_%p [shape=record label=\"{"
         "repeat | "
         "<%s> block | " "{ until | <%s> exp } " "}\"];\n", s, bf, ef);

    draw_edge(s, ef, s->exp);
    visualize_exp(OUTPUT_WRITER, s->exp);
    if (s->statlist) {
        visualize_statlist(OUTPUT_WRITER, s->statlist, s, bf);
    } else {
        emit(LITERAL_NULL, &s->statlist);
        draw_edge(s, bf, &s->statlist);
    }
}

void visualize_stat_cond(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_cond *s = (__typeof__(s)) st;
    const char *cf = "condlist";
    emit("_%p [shape=record label=\"" "<%s> condition list" "\"];\n", s, cf);
    visualize_condlist(OUTPUT_WRITER, s->condlist, s, cf);
}

void visualize_stat_func(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_func *s = (__typeof__(s)) st;
    const char *name_flag = "funcname";
    const char *body_flag = "funcbody";

    emit("_%p [shape=record label=\"{"
         "function expression"
         " | { <%s> funcname | <%s> funcbody } "
         "}\"];\n", s, name_flag, body_flag);
    visualize_dottedname(OUTPUT_WRITER, s->funcname, s, name_flag);
    draw_edge(s, body_flag, s->funcbody);
    visualize_funcbody(OUTPUT_WRITER, s->funcbody);
}

void visualize_stat_leave(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_leave *s = (__typeof__(s)) st;
    emit(LITERAL_SIMPLE_STATEMENT, s, "return");
    if (s->explist)
        visualize_explist(OUTPUT_WRITER, s->explist, s, NULL);
}

void visualize_stat_assign(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_assign *s = (__typeof__(s)) st;
    const char *setlist_flag = "setlist";
    const char *explist_flag = "explist";

    emit("_%p [shape=record label=\""
         "<%s> varlist | = | <%s> explist"
         "\"];\n", s, setlist_flag, explist_flag);

    visualize_setlist(OUTPUT_WRITER, s->setlist, s, setlist_flag);
    visualize_explist(OUTPUT_WRITER, s->explist, s, explist_flag);
}

void visualize_setlist(OUTPUT_CLOSURE, struct AstNode_setlist *s, void *from, const char *flag)
{
    struct AstNode_setlist *p, *next;
    int count;

    void *root = (void *) &p->link;
    emit("_%p [shape=record label=\"", root);

    count = 0;
    p = s;
    do {
        next = NEXT(p);
        emit("<_%p> setlist%d ", p, count++);
        p = NEXT(p);
        if (next == s)
            break;
        emit(" | ");
        p = next;
    } while (1);
    emit("\"];\n");

    p = s;
    char buffer[BUFSIZE];
    do {
        snprintf(buffer, BUFSIZE, "_%p", p);
        visualize_varlist(OUTPUT_WRITER, p->varlist, root, buffer);
        p = NEXT(p);
    } while (p != s);
}

void visualize_stat_funcall(OUTPUT_CLOSURE, struct AstNode_statlist *st)
{
    struct AstNode_stat_funcall *s = (__typeof__(s)) st;
    emit("_%p [shape=box label=\"call function\"];\n", s);
    visualize_varlist(OUTPUT_WRITER, s->funcall, s, NULL);
}

void visualize_explist(OUTPUT_CLOSURE, struct AstNode_explist *e, void *from, const char *flag)
    /* flag can be NULL  */
{
    struct AstNode_explist *p = e, *next;
    while (1) {
        next = NEXT(p);
        draw_edge(from, flag, p->exp);
        visualize_exp(OUTPUT_WRITER, p->exp);
        if (next == e)
            break;
        p = next;
    }
}

void visualize_exp(OUTPUT_CLOSURE, struct AstNode_exp *exp)
{
    const char *operator_sign = (const char *) NULL;    // for C++ compatiable
    switch (exp->type) {
    case EXP_NIL:
        emit(LITERAL_STR, exp, "nil");
        break;
    case EXP_TRUE:
        emit(LITERAL_STR, exp, "true");
        break;
    case EXP_FALSE:
        emit(LITERAL_STR, exp, "false");
        break;
    case EXP_VARARG:
        emit(LITERAL_STR, exp, "...");
        break;
    case EXP_NUMBER:
        do {
            struct AstNode_exp_number *e = (__typeof__(e)) exp;
            double dn = fabs(e->number);
            int isinteger = ((dn - (long long) dn) < 0.0000001);
            if (isinteger) {
                emit("_%p [ "
                     "shape=\"triangle\" label="
                     "\"%.0lf\" fontcolor=blue];\n", e, e->number);
            } else {
                emit("_%p [ "
                     "shape=\"triangle\" label="
                     "\"%f\" fontcolor=blue];\n", e, e->number);
            }
        } while (0);
        break;
    case EXP_STRING:
        do {
            tinaU_String *c = tinaU_quoteStr(((struct AstNode_exp_string *)
                                              exp)->string);
            emit(LITERAL_LITERAL, exp, c->buf);
            tinaU_destroyStr(c, NULL);
        } while (0);
        break;
    case EXP_PREFIX:
        emit(LITERAL_STR, exp, "prefix expression");
        visualize_varlist(OUTPUT_WRITER
                        , ((struct AstNode_exp_prefix *) exp)->prefixexp
                        , exp, NULL);
        break;
    case EXP_TABLECONSTRUCTOR:
        emit("_%p [shape=record label=\""
             " {  <e> table constructor } " "\"];\n", exp);
        do {
            struct AstNode_exp_tableconstructor *e = (__typeof__(e)) exp;
            if (e->fieldlist) {
                visualize_fieldlist(OUTPUT_WRITER, e->fieldlist, exp, "e");
            } else {
                emit(LITERAL_NULL, &e->fieldlist);
                draw_edge(exp, "e", &e->fieldlist);
            }

        } while (0);
        break;
    case EXP_FUNCTION:
        emit(LITERAL_STR, exp, "anonymous function expression");
        draw_edge(exp, NULL, ((struct AstNode_exp_function *) exp)->function);
        visualize_funcbody(OUTPUT_WRITER
                , ((struct AstNode_exp_function *) exp)->function);
        break;
    case EXP_UNARY_NOT:
        operator_sign = "-";
        goto unary;
    case EXP_UNARY_MINUS:
        operator_sign = "-";
        goto unary;
    case EXP_UNARY_POUND:
        operator_sign = "#";
        goto unary;
      unary:
        emit("_%p [shape=record label=\""
             "%s | <e> expression " "\"];\n", exp, operator_sign);
        draw_edge(exp, "e", ((struct AstNode_exp_unary *) exp)->exp);
        visualize_exp(OUTPUT_WRITER, ((struct AstNode_exp_unary *) exp)->exp);
        break;
    case EXP_OR:
        operator_sign = "or";
        goto binary;
    case EXP_AND:
        operator_sign = "and";
        goto binary;
    case EXP_LT:
        operator_sign = "<";
        goto binary;
    case EXP_GT:
        operator_sign = ">";
        goto binary;
    case EXP_LE:
        operator_sign = "<=";
        goto binary;
    case EXP_GE:
        operator_sign = ">=";
        goto binary;
    case EXP_EQ:
        operator_sign = "==";
        goto binary;
    case EXP_NE:
        operator_sign = "~=";
        goto binary;
    case EXP_CONCAT:
        operator_sign = "..";
        goto binary;
    case EXP_PLUS:
        operator_sign = "+";
        goto binary;
    case EXP_MINUS:
        operator_sign = "-";
        goto binary;
    case EXP_TIMES:
        operator_sign = "*";
        goto binary;
    case EXP_DIVIDE:
        operator_sign = "/";
        goto binary;
    case EXP_MOD:
        operator_sign = "%";
        goto binary;
    case EXP_EXPONENT:
        operator_sign = "^";
        goto binary;
      binary:
        emit(LITERAL_OPERATOR, exp, operator_sign);
        visualize_exp(OUTPUT_WRITER
                , ((struct AstNode_exp_binary *) exp)->exp[0]);
        draw_edge(exp, NULL, ((struct AstNode_exp_binary *) exp)->exp[0]);
        visualize_exp(OUTPUT_WRITER
                , ((struct AstNode_exp_binary *) exp)->exp[1]);
        draw_edge(exp, NULL, ((struct AstNode_exp_binary *) exp)->exp[1]);
        break;
    default:
        fprintf(stderr, "%d\n", exp->type);
        fflush(stderr);
        assert(0);
    }
}

void visualize_namelist(OUTPUT_CLOSURE, struct AstNode_namelist *n
                    , void *from, const char *flag)
{
    draw_edge(from, flag, n);
    emit("_%p [shape=record label=\" %s ", n, n->name);
    struct AstNode_namelist *p;
    for (p = NEXT(n); p != n; p = NEXT(p)) {
        emit("| %s", p->name);
    }
    emit("\"];\n");
}

void visualize_funcbody(OUTPUT_CLOSURE, struct AstNode_funcbody *funcbody)
{
    const char *pf = "params";
    const char *bf = "block";

    emit("_%p [shape=record label=\"{", funcbody);
    emit(" function body | {");
    emit("<%s> params | ", pf);
    emit("<%s> block ", bf);
    emit("}}\"];\n");

    visualize_params(OUTPUT_WRITER, funcbody->params);
    draw_edge(funcbody, pf, funcbody->params);
    visualize_statlist(OUTPUT_WRITER, funcbody->statlist, funcbody, bf);
    draw_edge(funcbody, bf, funcbody->statlist);
}

void visualize_dottedname(OUTPUT_CLOSURE, struct AstNode_namelist *n
                    , void *from, const char *flag)
{
    draw_edge(from, flag, n);
    emit("_%p [shape=record label=\" %s ", n, n->name);
    struct AstNode_namelist *p;
    for (p = NEXT(n); p != n; p = NEXT(p)) {
        if (p->name) {
            emit("| %s", p->name);
        } else {
            emit("| : %s", NEXT(p)->name);
            assert(NEXT(NEXT(p)) == n);
            break;
        }
    }
    emit("\"];\n");
}

void visualize_varlist(OUTPUT_CLOSURE, struct AstNode_varlist *v
                        , void *from , const char *flag)
{
    /* first item can be
     * 1. name:  a.b.c
     * 2. exp, (exp).a.b.c */

    /* intermediate item can be
     * 1. name: prefixexp.a.b
     * 2. [exp]: prefiexexp[12][13]  */

    /* extra last item can be
     * 1. args: prefiexexp(args)
     */

    // mount below the parent
    void *container = (void *) &v->link;
    draw_edge(from, flag, &v->link);

    // begin declare varlist itself
    emit("_%p [shape=record label=\"", container);
    // handle the first node of varlist
    struct AstNode_varlist *p = v;
    assert(p->type == VAR_NAME || p->type == VAR_PREFIXEXP);
    if (p->type == VAR_NAME) {
        emit("<_%p> %s", p, ((struct AstNode_var_name *) p)->name);
    } else {                    // if (p->type == VAR_PREFIXEXP) {
        emit("<_%p> expression ", ((struct AstNode_var_prefixexp *) p)->exp);
    }

    // handle intermediate node (and extra node)
    for (p = NEXT(p); p != v; p = NEXT(p)) {
        switch (p->type) {
        case VAR_PREFIXEXP:
            /* can not appear here  */
            assert(0);
            break;
        case VAR_NAME:
            emit(" | <_%p> %s", p, ((struct AstNode_var_name *) p)->name);
            break;
        case VAR_SUBSCRIPT:
            emit(" | <_%p> [ expression ]",
                 ((struct AstNode_var_subscript *) p)->exp);
            break;
        case VAR_FUNCALL_EMPTY:
            emit(" | () ");
            break;
        case VAR_FUNCALL_TABLE:
            emit(" | <_%p> \\{ table constructor \\} ",
                 ((struct AstNode_var_funcall_table *) p)->fieldlist);
            break;
        case VAR_FUNCALL_STRING:
            do {
                struct AstNode_var_funcall_string *pv = (__typeof__(pv)) p;
                if (pv->string[0]) {
                    tinaU_String *c = tinaU_quoteStr(pv->string);
                    emit(" | \\\"%s\\\" ", c->buf);
                    tinaU_destroyStr(c, NULL);
                } else {
                    emit(" | \"%s\" ", pv->string);
                }
            } while (0);
            break;
        case VAR_FUNCALL_EXP:{
                struct AstNode_var_funcall_exp *pv = (__typeof__(pv)) p;
                emit(" | <_%p> expression ", &pv->explist);
            }
            break;
        }
    }
    emit("\"];\n");             // end declare varlist itself

    // place children
    p = v;
    char buffer[BUFSIZE];
    if (p->type == VAR_PREFIXEXP) {
        struct AstNode_exp *e = ((struct AstNode_var_prefixexp *) p)->exp;
        snprintf(buffer, BUFSIZE, "_%p", e);
        draw_edge(container, buffer, e);
        visualize_exp(OUTPUT_WRITER, e);
    }
    for (p = NEXT(p); p != v; p = NEXT(p)) {
        switch (p->type) {
        case VAR_FUNCALL_STRING:
        case VAR_FUNCALL_EMPTY:
        case VAR_NAME:
            /* do nothing */
            break;
        case VAR_PREFIXEXP:
            /* can not appear here  */
            assert(0);
            break;
        case VAR_SUBSCRIPT:{
                struct AstNode_var_subscript *pv = (__typeof__(pv)) p;
                snprintf(buffer, BUFSIZE, "_%p", pv->exp);
                draw_edge(container, buffer, pv->exp);
                visualize_exp(OUTPUT_WRITER, pv->exp);
            }
            break;
        case VAR_FUNCALL_TABLE:{
                struct AstNode_var_funcall_table *pv = (__typeof__(pv)) p;
                snprintf(buffer, BUFSIZE, "_%p", pv->fieldlist);
                visualize_fieldlist(OUTPUT_WRITER, pv->fieldlist, container, buffer);
            }
            break;
        case VAR_FUNCALL_EXP:{
                struct AstNode_var_funcall_exp *pv = (__typeof__(pv)) p;
                snprintf(buffer, BUFSIZE, "_%p", &pv->explist);
                if (pv->explist) {
                    visualize_explist(OUTPUT_WRITER, pv->explist, container, buffer);
                } else {
                    emit(LITERAL_NULL, &pv->explist);
                    draw_edge(container, buffer, &pv->explist);
                }
            }
            break;
        }
    }
}

void visualize_params(OUTPUT_CLOSURE, struct AstNode_params *p)
{
    const char *extra_tail = p->vararg ? " | ..." : "";
    if (p->namelist) {
        emit("_%p [shape=record label=\"<n> parameters %s\"];\n", p,
             extra_tail);
        visualize_namelist(OUTPUT_WRITER, p->namelist, p, "n");
    } else {
        emit("_%p [shape=record label=\"<n> parameters %s\"];\n", &p->namelist,
             extra_tail);
        emit(LITERAL_NULL, &p->namelist);
        draw_edge(p, "n", &p->namelist);
    }
}

static void visualize_cond(OUTPUT_CLOSURE, struct AstNode_condlist *c)
{
    emit("_%p [shape=record label=\"", c);
    emit("<c> cond | <b> block");
    emit("\"];\n");
    draw_edge(c, "c", c->exp);
    visualize_exp(OUTPUT_WRITER, c->exp);
    visualize_statlist(OUTPUT_WRITER, c->statlist, c, "b");
}

void visualize_condlist(OUTPUT_CLOSURE, struct AstNode_condlist *c
                    , void *from, const char *flag)
{
    /* mount  */
    draw_edge(from, flag, &c->link);

    struct AstNode_condlist *p;
    emit("_%p [shape=record label=\"", &c->link);
    emit("<_%p> if", c);
    for (p = NEXT(c); p != c; p = NEXT(p)) {
        if (p->exp)
            emit("| <_%p> elseif", p);
        else {
            emit("| <_%p> else", p->statlist);
            assert(NEXT(p) == c);
        }
    }
    emit("\"];\n");             // end condlist declaration

    // handle each cond
    /*  if clause */
    emit("_%p:_%p -> _%p;\n", &c->link, c, c);
    visualize_cond(OUTPUT_WRITER, c);
    for (p = NEXT(c); p != c; p = NEXT(p)) {
        if (p->exp) {           /* elseif clause  */
            emit("_%p:_%p -> _%p;\n", &c->link, p, p);
            visualize_cond(OUTPUT_WRITER, p);
        } else {                /* else clause  */
            char buf[BUFSIZE];
            snprintf(buf, BUFSIZE, "_%p", p->statlist);
            visualize_statlist(OUTPUT_WRITER, p->statlist, &c->link, buf);
        }
    }
}

void visualize_fieldlist(OUTPUT_CLOSURE, struct AstNode_fieldlist *f
                        , void *from, const char *flag)
{
    void *main_container = (void *) &f->link;
    draw_edge(from, flag, main_container);
    emit("_%p [shape=record label=\"\\{", main_container);

    struct AstNode_fieldlist *p = f;
    do {
        if (p->type == FIELD_ONLY_VALUE) {
            struct AstNode_fieldlist_only_value *tmp = (__typeof__(tmp)) p;
            emit(" | <_%p> %s | , ", tmp->exp, "value");
        } else if (p->type == FIELD_INDEX_KEY) {
            struct AstNode_fieldlist_index_key *tmp = (__typeof__(tmp)) p;
            emit(" | <_%p> %s | = |<_%p> %s | ,",
                 tmp->index, "[key]", tmp->exp, "value");
        } else if (p->type == FIELD_NAME_KEY) {
            struct AstNode_fieldlist_name_key *tmp = (__typeof__(tmp)) p;
            emit(" | %s | = |<_%p> %s | ,", tmp->name, tmp->exp, "value");
        }
        p = NEXT(p);
    } while (p != f);
    emit("| \\}\"];");

    p = f;
    char buffer[BUFSIZE];
    do {
        if (p->type == FIELD_ONLY_VALUE) {
            struct AstNode_fieldlist_only_value *tmp = (__typeof__(tmp)) p;
            snprintf(buffer, BUFSIZE, "_%p", tmp->exp);
            draw_edge(main_container, buffer, tmp->exp);
            visualize_exp(OUTPUT_WRITER, tmp->exp);
        } else if (p->type == FIELD_INDEX_KEY) {
            struct AstNode_fieldlist_index_key *tmp = (__typeof__(tmp)) p;
            snprintf(buffer, BUFSIZE, "_%p", tmp->index);
            draw_edge(main_container, buffer, tmp->index);
            visualize_exp(OUTPUT_WRITER, tmp->index);

            snprintf(buffer, BUFSIZE, "_%p", tmp->exp);
            draw_edge(main_container, buffer, tmp->exp);
            visualize_exp(OUTPUT_WRITER, tmp->exp);
        } else if (p->type == FIELD_NAME_KEY) {
            struct AstNode_fieldlist_name_key *tmp = (__typeof__(tmp)) p;
            snprintf(buffer, BUFSIZE, "_%p", tmp->exp);
            draw_edge(main_container, buffer, tmp->exp);
            visualize_exp(OUTPUT_WRITER, tmp->exp);
        }
        p = NEXT(p);
    } while (p != f);
}


void tinaA_generate_graph(tinaA_Root *root
                        , void *output_opaque
                        , tina_OuputFunc output_func)
{
    visualize(root, output_opaque, output_func);
}
