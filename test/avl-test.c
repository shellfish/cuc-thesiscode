#include "tavl.h"
#include <string.h>
#include <stdio.h>

int main(void)
{
    tinaU_Comparer comp = (tinaU_Comparer)strcmp;
    tinaU_AvlTree t;
    tinaU_initAvl(&t);
    t.comparer = comp;

    char a[] = "hello world";
    tinaU_AvlNode *n = tinaU_avlReplace(&t, a);
    if (n->data == NULL)
        n->data = a;

    tinaU_AvlNode *b = tinaU_avlFind(&t, a);
    printf("%s\n", b->data == (const void*)a
            ? a
            : NULL);

    tinaU_freeAvl(&t);
    return 0;
}
